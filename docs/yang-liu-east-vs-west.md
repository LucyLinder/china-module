
__Lifestyle: Independent vs. dependent__

![Lifestyle: Independent vs. dependent](http://media.bsix12.com/2012/03/lebensstil.jpg)

__Attitude towards punctuality__

![Attitude towards punctuality](http://media.bsix12.com/2012/03/puenktlichkeit.jpg)

__At a party__

![At a party ](http://media.bsix12.com/2012/03/party.jpg)

__Ideal of beauty__

![Ideal of beauty](http://media.bsix12.com/2012/03/schoenheitsideal.jpg)

__Elderly in day to day life__

![Elderly in day to day life](http://media.bsix12.com/2012/03/elderly-in-day-to-day-life.jpg)

__The boss__

![The boss](http://media.bsix12.com/2012/03/chef.jpg)

__Noise level inside a restaurant__

![Noise level inside a restaurant](http://media.bsix12.com/2012/03/restaurant.jpg)

__Problem-solving approach__

![Problem-solving approach](http://media.bsix12.com/2012/03/umgang-mit-problemen.jpg)

__Size of the individual’s ego__

![Size of the individual’s ego](http://media.bsix12.com/2012/03/eastwest-ego.jpg)

__Perception: How Germans and the Chinese see one another__

![Perception: How Germans and the Chinese see one another](http://media.bsix12.com/2012/03/east-vs-west.jpg)

__How to stand in line__

![How to stand in line](http://media.bsix12.com/2012/03/queue.jpg)

__Complexity of self-expression__

![Complexity of self-expression](http://media.bsix12.com/2012/03/meinung.jpg)

__Traveling and recording memories__

![Traveling and recording memories](http://media.bsix12.com/2012/03/traveling.jpg)

__Connections and contacts__

![Connections and contacts](http://media.bsix12.com/2012/03/contacts.jpg)

__Three meals a day__

![Three meals a day](http://media.bsix12.com/2012/03/drei-mahlzeiten.jpg)

__Animals__

![Animals](http://media.bsix12.com/2012/03/tiere.jpg)

__Anger__

![Anger](http://media.bsix12.com/2012/03/anger.jpg)

__Moods and weather__

![Moods and weather](http://media.bsix12.com/2012/03/moods-and-weather.jpg)

