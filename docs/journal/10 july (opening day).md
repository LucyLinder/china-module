# 10/07 : welcome at iCorsi

## Some practical information

beware of water and prefer canned bottles ! Ice is a false friend because it is often made out of tapwater...

Food in the south part if very hot/spicy. Beware of red pepper, don't eat too much, it is very strong in the stomach. Never eat raw.

In the restaurant, we order for everybody: each plate is huge enough or you will get one plate of the order for everyone. So be careful : _one order for everybody_!

The organisers need to take gifts for every enterprise to show respect and such. We need to take them with us (40 students), since one luggague would not be enough. 

Use China Unicom. It is possible to use "usb sticks" for portable hotspots.