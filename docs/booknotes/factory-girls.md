The book _factory girls_, by Leslie T. Chang, has been published in 2008. 

> Chang, a Chinese-American former correspondent for the Wall Street Journal in Beijing, spent several years researching this report of modern-day China, and the young women migrant workers who leave their small rural villages to go to work in the big-city factories. 

> This book shows that the function of factory work and the situation of rural workers in urban factories has changed significantly from the early studies of the 90s. 

In this page, I won't describe the book or my opinion, but rather gather some passages that amazed me during my lecture.

## A bit of history

A __1984__ directive allowed farmers to settle in small market towns --> suddenly, to be on the move was no longer a crime. Migration picked up speed and by __1990__, the country had 60 mio migrants, _the largest migration in human history_. By __2003__, the State Council published a document that banned job discrimination against migrants and advocated better working conditions for them + schooling for their children. 

With all this, migration is emptying villages of young people. Most countryside villages are simply dying.

## Conditions of factory-line workers

* Most girls slept in the factory dorms, often more than twelve in a room
* The salary is very low and the payday is even smaller as money is deducted for silly and small things: being late one morning, taking a half-day off because of sickness, having to pay extra for uniforms, etc. (see p. 4)
* you a required to stay six months, and even then the permission to quit is not always granted --> many just disappear without further notice, leaving behind two month worth of money (employers keep every worker's first two salaries)
* a usual day is 13 hours on the job with two short breaks for meals. Some factories offer one or two days off a week, but not everywhere.
* talking on the job --> 5 yuan fine
* bathroom breaks limited to 10 minutes and require a signup list
* in Dongguan, the estimation is 70% female of young/unskilled workers.
* job mobility is high, contrary to the reality of the 90s
* for Min: "it looks all right from the outside, but you never see the inside until you've aggreed to work there. And then you have nowhere else to go"
* "Do you see how they are all unhappy when they get their month's pay ? They are all thinking: I work so hard and this is all I get ?" (p. 80)
* injuries closely tighed to the demands of production: in the spring, no more time for training.
* stress depends on the place in the line (p. 113)
* Brands like Nike or Adidas began pushing suppliers to improve the working environment, but at the same time, the also pressured them to cut costs... p. 114
* you often rise thourgh speaking up and telling lies

## the talent market 

Discrimination as operative rule (p. 90): gender, origin, size. _Height_ is a universal Chinese obsession: in a country that experienced malnutrition and famines, height signaled fortune and functioned as a proxy for class. Age is another liability: 35 is an upper limit for many positions.

Interviews are short and brutally honest.

## Gender differences

There is a strong gender segregation (p. 56). 

Inside factories, women are a the assembly line and in human resources (clerks), men are at the top or the dead-end bottom positions: bosses versus guards, drivers, etc. Outside factories, women are waitresses, nannies, hairdressers and prostitutes, men work on construction sites.

Women also enjoy a more fluid job situation: from the assembly line, they move up to clerks or salespeople. Young men have a harder time entering a factory and once there they stuck.

Why ? Huge differences in mentality and incentives.

> In one survey, men cited higher income as the chief purpose of leaving home, while women aspired to "more experience in life". Unlike men, women had no home to go back to [Since traditionnally women move to the village of their husband, ] a son would forever have a home in the village where he was born. Daughters, once women, would never return home to live -- until they married, they didn't belong anywhere.

In a sense, this deep-rooted sexism worked in in women's favor: precisely because they mattered less, they were freeer to do what they wanted. 

## Changes in migrant profiles

see p. 105.

1980s-1990s: migrants were heading to the unknown, often driven by a family's need for cash and the desire to build a house back home. It was considered risky, even shameful, for a single woman to go out on her own. Early migrants often found seasonal work; when they made enough money, they returned home for good.

200s: the new generation came of age when migration was already an accepted path to a better life. Younger and better educated, they are driven out less by the poverty of the countryside than by the opportunity of the city. There is no longer any shame attached to migration. The shame now lies in staying home. Newer migrants also have looser ties with their villages. They come home whenever they liked, mostly according to the factories schedule instead of the schedules of the fields. They are more ambitious and less content than their elders were: their basis of comparison is now the city.

Now, migration has become the chief source of village income. 

## Their values 

* _chuqu_ = to go out. It is what defines their lives: "_there was nothing to do at home, so I went out._" (p. 11)
* often share a common history: a blurry arrival in the city, a beginning where she is tricked in some way. If there is a turning point, it always came when she challenged her boss.
* part of being a migrant worker was having no idea how to spend leisure time (mostly because of the short of money) (p. 78)
* individualism and self-improvement are the life-motives.
* everyone in the city live on fast-forward
* _dakuan_ = self-made businessmen whose wealth carries a strong whiff of corruption

## Relations: home, friendship

* relations between locals and migrants: "mutual contempt"
* in the village, relations are determined by kinship ties and shared histories. In school and on the assembly line, everyone is in the same lowly position. But once a person starts to rise in the factory world, the balance of power shifts, making existing relationships difficult.
* in migrant magasines, the moral is always the same: you can only rely on yourself. This message of self-reliance goes too far sometimes (p. 62)
* "I saw more charity toward beggars in Donguan than I ever saw in another Chinese city [...] but toward people their own age they showed no pity": if you are young and healthy, there is no excuse for not working (p. 77)
* A dating profile includes characteristics such as political identity, apartment ownership and the healrth and financial standing of one's family members (p. 213). People are often not looking for love, but have pragmatic concerns
* don't be fooled: there are many divorces and men cheat on their wives
* money -> family power (p. 371)

## In the villages

* Hierarchy governs village lifre. Children often sleep with their parents in one large bed (a _kang_); there are no secrets in the village
* in village homes, it is common to throw trash, put out cigarettes and spit on the floor; every so often, someone sweeps up the mess and dumps it in the yard.
* the hierarchy of the universe: heaven -> earth -> nation -> parents -> teachers.
* birth order was a major determinant of fate. The first is a cash-machine, the nexts have more chance to finish school.
* people spend all their time in groups, so they are ood at ignoring one another. Almost everything is done communally (p. 289)
* the focus of village life is television. TV is on all day long. Few read newspapers or watch the news.
* the Chinese countryside is not relaxing: it is a place of constant socializing and negotiation (p. 293)

## Genealogy

see p. 322.

Families trace their lineage back to the _shiqianzu_, the first migrating ancestor. Only the records after him are considered authentic. 

The list of crimes leading to expulsion from the genealogy is long. According to a set of rules published during the Qing Dynasty, it included: violating ancestral graves, marrying in disregard of social classes, violent rampaging, whoring, joining armed rebellion, trason or heresy, whithholding the truth from the emperor, marrying a prostitute, actress, slave or servant. 

> The genealogy reflected the traditional Chinese view that the purpose of history was not to relate facts or record stories, but to establish a moral standard to guide the living. History was not simly what happended, but what ought to happen if people behaved as they should.

## Misc

* Counterfeit diplomas and ID cards are available for sale at street corners
* no schedule of maps for buses, drivers can decide to stop suddenly and reimburse you 10 yuan. You just leave the bus at the middle of nowhere.
* in the mid-90s, a craze for network sales swept China (p. 66), marking the rise of pyramid schemes. In 1998, the cabinet of Premier Zho Rongji ordered all _chuanxiao_ to cease operations. More than two thousand companies shut down in an instant.  
* Drinking is a big part of workplace socializing in China. Drinking is work: how to drink, what to drink, train to support alcohol (p. 185).
* marriage is a filial duty (p. 207). In China, a date is a group activity
* People are terrified of being singled out, but from the safety of the group they could turn on someone with a speed and ferocity that takes your breath away (p. 209)
* journalism as extortion (p. 211)
* prostitution and hierarchy, p. 237
* Mr. Wu Assembly-Line English
* big ears is a sign of good fortune
* deciding to be a government official is just another way to go into business, since officials work together with the companies. Its a form of cooperation (p. 340)
* In a world where almost everyone worked nonstop, Chinese officials enjoyed two-hour breaks for lunch and a long nap.
* health is a national obsession. Most people did not have medical insurance and lived in fear of being bankrupted by illness. They loved to discuss ailments and folk cures; question about fertility or the state of one's bowels were considered perfectly normal (p. 305)


## Quotes

About the life in the City (p. 11):

> The city does not offer them an easy living. The pay for hard labor is low - often lower than the official minimum wage, which ranges between fifty and eighty dollars a month. Work hours frequently stretch beyond the legal limit of forty-nine hours per week. Get hurt, sick, or pregnant, and you're on your own. Local governments have little incentive to protect workers; their job is to keep the factory owners happy [...]. What keeps them in the city is not fear but pride: to return home early is to admit defeat. To go out and stay out - chuqu - is to change your fate.

About social interactions in China (p. 197):

> I have always that social interactions among the Chinese were needlessly complicated. Confucian tradition, which enphasized not the individual but his role in a comple hierarchical order, placed great value on status, self-restraint, and the proper display of respect. The Chinese had been living in densely populated communities for several thousand years, and they had developed subtle skills of delivering and detecting slights, eerting power through indirect means, and manipulating situations to their own benefit, all beneath a surface of elaborate courtesy. Even Chinese themselves often complained that living in their society was _lei_, tiring.

About scars of the past (p. 312):

> Almost everyone you met over a certain age had lost years to political campaigns; many were still paying a price in poor health, or lost education, or family members who had dided. But there was a surprising lack of bitterness. When you asked about individual experiences, people tended to retreat into the collective voice: _so many people suffered_ [...] In a world where so many people had suffered, one person's story did not matter.

About the relationship with the past (p. 316):

> The Chinese today have a troubled relationship with their past. On the surface, they take pride in it - _China has five thousand years of history_, one is constantly reminded as an American - but there is an aversion to going much deeper than the level of a Qing Dynasty television soap opera. [...] Forgetting and the instinct against introspection runs deep in this culture.

About corruption (p. 339):

> Kickbacks were universal in her industry. In order to make a sale, she usually had to pay 10% under the table to the buyer. [...] Corruption was embedded in the language. The word for "commissions", _yongjin_, meant a legitimate percentage earned by a seller, but it also referred to illegal kickbacks to a buyer. As far as the language was concerned, you couldn't distinguish between legal and illegal transactions, even if you wanted. 
> At the lunar new year, clients were given _hongbao_ - red envelopes of cash - and lavish boxes of tea, liquor, and cigarettes, which were called _li_, presents. I never heard the Chinese word for "bribe" used by anyone.

About numbers (p. 344):

> It seemed that everything in Dongguan could be reduced to numbers: sales, kickbacks, language ability. The height of a potential boyfriend. You started with numbers - What year are you ? How much a month ? How much for overtime ? - and then other numbers charted your progress: salary, the square-footage of an apartment, the price of a new car. 

About migrants nowadays (p. 383):

> [In the past], the concerns of family and nation were overwhelming, and they trapped a great many people - millions upon millions - in lives they never would have chosen. [...] 
> There was a lot to dislike about the migrant world of Min and Chunming: the materialism, the corruption, the coarseness of daily existance. But now there was an opportunity to leave your village and change your fate, to imagine a different life and make it real. [...] nowadays their purpose was not to change China's fate. They were concerned with their own destinies, and they made their own decisions. If it was an ugly world, at least it was their own.