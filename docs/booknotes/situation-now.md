
## Current migrant workers in China

[The China Labour Bulletin](http://www.clb.org.hk/content/migrant-workers-and-their-children){: target="_blank"} offers an overview of the current situation in China.

As their introduction explains:

> There are an estimated 282 million rural migrant workers in China, making up more than one third of the entire working population. Migrant workers have been the engine of China’s spectacular economic growth over the last three decades but they remain marginalized and subject to institutionalized discrimination. Their children have limited access to education and healthcare and can be separated from their parents for years on end.

### Hukou system

The **_hukou_ system** has been introduced in 1958. It is a houshold registration that holds information about citizens and give them access to the social welfare services, but also categorizes them as either _rural_ or _urban_ worker. Each town or city issues its own passport.

The hukou is hereditary and limits the possibilities of the rural population to move around. Indeed, a peasant wanting to move to a non-agricultural area (or even another county) needs to apply. The application process is slow and such moves are rarely granted.

The main goal of the hukou system was to ensure that the rural population would stay in the countryside, thus providing food and low cost workforce to the cities and the state owned businesses. It was mainly an instrument for economy and social stability.

More formally, the hukou system had 3 main goals (cf. [wikipedia](https://en.wikipedia.org/wiki/Hukou_system){: target="_blank"}):

1. _government welfare and resource distribution_: it is the basis for resource allocation, which in the facts favored urban centers with investment and subsidies;
2. _internal migration control_: it allows the government to control and regulate internal migration, since migrants need to apply for a new hukou;
3. _ criminal (and political) surveillance_: based on hukou files, the police maintains a list of targeted people to be monitored, controlled and restricted in their movements (isolation)

In the 1980s, the economic reforms took place and the cities needed more and more cheap labour. As a result, many peasant workers migrated anyway. In response, the city governments imposed discriminatory rules against peasants/migrants.

As the most impressive migration of all times took place, it became clear that the _hukou_ was counter-productive. It was unenforceable and a frein to economic growth. In 2003, the barriers to migration began to break down. The system dismantled in many small cities and the most obvious restrictions were abandoned.

Now, migration is easier for peasants, but the system itself is still very much in place, especially in major cities. For example, Beijing is making it more difficult for migrant workers and their families to get access to social services. 


## in 2015

There were 277.5 million rural workers in China's citites in 2015, making up about 36% of the total workforce. The rate of growth has decreased in the last decade, even if the number of migrant workers has increased steadily. 

The proportion of workers aged 16 to 30-years-old fell from 42 percent in 2010 to 33 percent in 2015, while the proportion of workers over 40-years-old has jumped from 34 percent in 2010 to 45 percent last year.

The __gender balance of migrant workers in 2015 was almost exactly two thirds male to one third female__. This contrasts markedly with the traditional image of migrant workers as “factory girls” employed in Pearl River Delta sweatshops.