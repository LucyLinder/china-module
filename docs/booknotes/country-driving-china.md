

Published in 2010, _Country Driving: A Journey from Farm to Factory_ is written by _Peter Hessler_, an American journalist and writer. He is actually married to Leslie Chang, with whom he has twin daughters.

The book is divided into three distinct stories. We first follow Hessler in a road trip through the northwestern part of China, which is pretty much devoid of industry and young people. The second part relates his life in the small village of Sancha, just a few hours from Beijing. The third part is another road trip, this time in an industrial zone. Most of this part evolves around bosses that try to make it in the world of nylon-coated rings for underwear and an underaged girl (and her family) that lie herself into factory jobs.

## Striking quotes

About today's mentality

> In China, much of life involves skirting regulations, and one of the basic truths is that forgiveness comes easier than permission (p.23)

About the Chinese pride

> To this day, this is the strength of the Chinese. It's not force. It's not that they've got spies or secret police. It's that there is something about being a part of the Chinese world that is appealing to the people around it (p.43)

<!-- -->

> At the root of the respect [towards foreigners who speaks the language] is insecurity: deep down, many Chinese, especially educated people, are slightly ashamed of the way their country might appear to an outsider. (p.213)

About change

> The place changes too fast; nobody can afford to be overconfident in his knowledge, and there's always some new situation to figure out. [...] nobody has today's China figured out. (p.59)

<!-- -->

> Back then, projects tended to be abstract and collective [...] But ever since Deng Xiaoping, the economy had been driven primarily by individual motivation, and rewards were suddenly tangible. And the new mobility meant that many people had caught a glimpse of a better life. (p.69) By learning more about other places, residents had lost touch with their immediate environment. And decades of political instability had warped people mindsets. "When things change so quicly, people don't have time to gain information about their environment". [...] Long-term planning made no sense: the goal was gain some profit today before you found yourself overwhelmed by the next wave of change. (p.70)

<!-- -->

> Sometimes they seemed to grasp instinctively at the worst of both worlds: the worst modern habits, the worst traditional beliefs.
> The longer I lived in China, the more I worried about how people responded to rapide change. [...] From what I saw, the nation's greatest turmoil was more personal and internal. Many people were searching; they longed for some kind of religious or philosophical truth, and they wanted a meaningful connection with others. They had trouble applying past experiences to current challenges. Parents and children occupied different worlds, and marriages were complicated - rarely did I know a Chinese couple who seemed happy together. It was all but impossible for people to keep their bearing in a country that changed so fast. (p.332) 

<!-- --> 

> In any case, life is complicated in China, and often there isn't a good solution regardless of how quick you are. The people have a common expression for that: _Mei banfa_, nothing can be done. (p.195)

<!-- -->

> Even as foreign materials pour into China, and young people seek out new influences, their instincts often remain deeply traditional (p.443)

About women

> When I was a little girl, I remember one of my aunts saying to my father, "Why should you pay for her to be educated ? She'll just marry into another family eentually. That means you're paying for the education of somebody else's family." (p.218) 

About education

> the strength of Chinese schooling consists of good intentions, the weakness lies in the details  (p.276)


About innovation and economy

> The bigger question is whether Chinese companies can move beyond low-margin products, developing industries that require creativity and innovation. In the end, this is the greatest contrast between China's boom and the history of the western Industrial Revolution. In Europe and the US, the rise of industry involved radical changes in thinking, and it happended partly because of a shortage of labor. [...] In today's China, though, there's little incentive t osave labor. [...] All of it - the high population, the lack of social institutions, the slowness of educational reform - combines to dull the edge of innovation. Inevitably, any nation is tempted to waste its greatest wealth, and in China this resource happens to be human. (p.457)

<!-- -->

> In 2009, China had traded overseas consumers for the ones at home. (p.527)

About individualism

> The degree of social mobility is higher than in most developing countries, and talent and hard work usually pay off. But such folks rely on the government for virtually nothing. They find agency elsewhere: they pay for private training courses; they learn to use guanxi; they switch jobs on their own. They negotiate hard with bosses, using any advantage they can find. If they're relocated to a dead  community, they simply leave. With so many options and so much movement, it doesn't make sens to get involved in a doomed battle against the cadres. (p.494)

<!-- -->

> I was most heartened by signs of individualism - way in which people had escaped the group mentality of the village, learning to make their own decisions and solve their own problems. But it would take another major step for such personal lessons to be applied to society-wide issues. Perhaps the final motivation would be economic - often I sensed that China needed to reach a point where the middle and upper classes felt like the system prevented them from succeeding. (p.495)




## First part: countryside road trip

Since the beginning of the __Reform and Opening__, the period of free market opening initiated by Deng Xiaoping in 1978, development has been most intense in the coastal regions. The whole country is moving in that direction: at the time of my journey, approximately ninety million people had already left the farms, mostly bound for the southeast, and the routines of rural life were steadily giving way to the rush of factory towns (p. 8).

The __Cultural Revolution__: political campaigns that lasted from 1966 to 1976, when Mao Zedong encouraged the Chinese to attack anything traditional and "feudal" (p.13)

In 1978, approximately 80% of the population lived in the countryside. By 2001, an estimated ninety million had already left home. It is the largest migration in human history - nearly one tenth of the population was on the road, finding new lives away from home. (p.40)

### Miscellaneous

* At one point, the Chinese had even considered converting the Great Wall into a highway (p.9)  
* In these crowded landscapes, everything was a potential resource. Hills have been carved into crop terraces; roads serve to dry vegetables; passig cars double as threshers (p.13)
* the physicality of pedestrians - I was constantly getting bumped and pushed. In a city of thirteen million you learn to expect contact, and driving works the same way (p.21)
* sometimes if a driver hits a big dog, he just throws it in the trunk, takes it home and cooks it (p.22)
* some parts of the west were closed conpletely to outsiders, because of poverty, ethnic tensions, or military installations. And a foreign journalist was technically required to apply to local authorities before traveling anywhere in the country. 
* to them, it wasn't Chinese civilization, it _was_ civilization (p.42).
* more than one-fourth of China's land suffers from desertification, and the total area of stricken regions expands by an estimated 1,300 square miles every year.  
* _Strange stones_ is the Chinese term for any rock whose shape resembles something else. It's an obsession at scenic destinations accross the country (p.90)
* In ancient China, race was essentially cultural, and a person who lived among barbarians could lose this "Chineseness" (for example, many Chinese talk about Genghis Khan as if he were Chinese) (p.103)
* a village is is for the very old, the disabled and the very young, because migrants left their children behind to be raised by grandparents (p.114).
* in the capital, nearly everybody is an only child, coddled and spoiled from birth. (p.117)
* Chinese crowds behave in unpredictable ways. The final direction is never easy to anticipate, because it depends largely on whether some dominant personality emerges within the group. A single ouspoken person can sway an entire incident, inspiring the crowd to action. (p.119)
* Chinese complaints are highly contagious. It has something to do with the group impulse, and people can't seem to help themselves - if they see others behaving a certain way, they immediately catch the vibe. (p.471)
* Chinese can be passive about many things, but food is not one of them. (p.470)

> What does it mean when the Great Wall becomes a cell phone accessory ? Or when computer discs are most useful because they bounce light ? Everything was tangled in these parts; there was no distinction between progress and improvisation. (p.118)

### About foreign trade

Read pages 77 to 82 for a history of foreign trade... Fascinating !

### About the government

* the Chinese government is amazing with numbers, and it always has been. Since the reform years, this age-old tradition has helped make China an ideal client for the World Bank. The government can mobilize labor; it can produce statistics; and it can pay loans back (0.48)
*  people don't know what the country's leaders really say. [for them], local leaders are the biggest problem - the county officials are the ones who embezzle everything (p.51)
*  _shan gao huangdi yuan_, the mountains are high, the emperor far away is common in rural China. People invariably believe that problems are local. It is rare to meet somebody who is cynical about the system to its core. (p.52)
* the Communist Party allows only five official religions: Buddhism, Daoism, Islam, Catholicism and Protestantism. All faiths are monitored by government agencies, and ther's no tolerance for independent leadership (catholics can't recognize the Pope) (p.255)
* the Party might be bankrupt, but it's still incredibly well organized and coordinated. And the Party understands the significance of local power in a nation that's mostly rural. There are essentially no rural residents who live beyond the reach of village politics. 
* In places like Sancha, real local power is held by the members of the Communist Party. (p.239)
* The communist require a formal application, followed by meetings and interviews; local members habe the authority to reject anybody they deem inadequate. And membership is rare: roughly 5% of the population are card-carrying Communists. (p.262)
* workers generally have little faith in government - "You have to handle these things yourself" (p.449)
* in factory towns, the most common attitudes toward the gobernment range from scorn to complete disinterest. Many people complain about official corruption, but they tend to do so in abstract terms, because they have little direct contact with cadres ... Authoritites usually find strategies to earn money without making it too personal. 
* Examples of the way of the government with the Dam and relocation p.492
* virtually all complaints occur at this level: they're intensely local and individual (p.493)

## About the Great Wall

* at least ten distinct words were used for what we know think of as "Great Wall". (p.27)
* There isn't a single scholar at any university in the world who specializes in the Great Wall. Ming defense work are networks ratger than single structures.(p.28)
* eventually the myths proved appealing to nationalists like Mao, who used the Great Wall in propaganda, recognizing the symbolic value of a unified barrier. Nowadays, there is even a single term, _Changcheng_, literally "long wall" (p.28).
* 

### About roads

* They don't name small road: only highways were numbered, not capillaries (p.9)
* in China, the law requires most citizens to be cremated, only outlying rural regions are allowed to conduct burials. Their mourning clothes are white (p.14)
* Beginning in 1998, the government had invested heavily in rural roads, partly as a response to the Asian financial crisis. In modern China, road building has often been a strategy for dealing with poverty or crisis (p.18)
* In 2001, the ratio was 128 people for every vehicle, similar to the US in 1911 (p.19)
* China had about one-fifth the number of cards and buses as the US, but there were more than twice as many traffic fatalities (p.33)
* the transition has been so abrupt that many traffic patterns come directly from pedestrian life; they rely on automobile body language (p.34)
* the use of headlights was banned in Beijing until the late 1970s (because officials made several roadtrips abroad) (p.35) 
* it's hard to imagine another place where people take such joy in driving so badly
* a lot of drinking-and-driving peer pressure
* during driving lessons, nobody ever wore a seat belt. They had beer during the driving class. (p.63)
* Someone said to him: I can't believe you're driving in this country. He responded: I can't believe you get into cabs and buses driven by graduates of Chinese driving courses (p.64)
* By law, an applicant for a truck or bus driver's license must be younger than fifty. For regular cars, the limit is 70. (p.88)
* the government also controles the fuel industry, which means that even in remote areas you can find a gas station. price controls kept gas cheap.(p.124)
* pumping gas was a women's work (p.124)
* China may have come late to the world of high-speed transport - the nation's first expressway wasn't completed until 1988 - but by 2020 they intended to have more highway miles than the US. (p.354)
* the potentital of speed traps: police officers invested as private stockholders in radar cameras [...] there were strict rules that governed police investment, But these were the rules of hierarchy and profit, not law and order. (p.448)

### About feng shui

> In ancient times, these beliefs often influenced military and political affairs. In 1949, they attacked many cultural traditions as superstituous, including religion, fortune-telling, and feng shui analysis. Even when the reforms of Deng Xiaoping introduced greater tolerance, some practices never recovered - Taoism, for example, attracts frew believers in today's China. But faith in feng shui has proven to be resilient, largely because it's connected to business (p.15)


### About maps

In ancient China, they relied heavily on words rather than symbols. Landscapes were warped to emphasize whatever happended to be of prime interest. You had for example huge towers loom atop steep cartoonish peaks, whereas the surroundings lack detail or scale. 

There are a number of reasons: a lack of government interest in exploration and trade; the fact that students are more encouraged to write about their environment than sketch it (no Atlas in class); the fact that when an army is in the middle of the desert, it is more important to stress big landscape clues than to have an exact cartography. 

Also, there's still a tendency to associate any mapmaking with limitary interests. Many topographic maps are classified and unavailable on the market. 

See page 126 and following.

--------------------------------

## Second part: the village

During the 1940-1950s, thousands of landlords were killed in cold blood. Fields were given to tenant farmers. Accross rural China, this initial stage of Communist land reform had an immediate effect. The new sense of ownership made farmers more likely to work hard, and in the early 1950s the nation's rural productivity increased, along with living standards. But these improvements turned out to be short-lived. 

During the second half of the 1950s, rural land was reorganized once more, this time into village communes. Farmers lors their new titles, as well as their right to individual profit. Everything was to be shared in common. [...] Without the possibility of personal gain, farmers lacked motivation, and rural poverty was still endemic in the 1970s. (p.234-238).

End of the story page 236 and following.

In Chinese cities, all land still technically belonged to the government, but private individuals were gien the right to buy and sell dwellings. The couldn't own the land, but they could own the building or apartment atop the land. But in the countryside, an individual can't buy or sell his farmland, and he can't mortgage it. [...] The law gives cities and townships the right to acquire any suburban land that's in the "public interest", a term that's never been defined, and the result is that urban places expand at will. During the period 1990-2002, 66 million farmers lost their land. (p.237)

### Miscellaneous

* the saving grace of Chinese staring: people never glanced asway in embarrassment when you caught them looking, and it was hard not to respect such open curiosity. (p.163)
* Chinese police can be brutal, but usually they're as pragmatic as everybody else in the country. Quite often their primary goal is to be absolved of any responsibility whatsoever. (p.167)
* three times a day, the village propaganda speakers broadcast local announcements, county news and national events (p.178)
* historical soap operas are nowadays how many rural Chinese learn history. (p.182)
* the Chinese planned birth policy is heavily localized, depending on geography and ethnicity. It requires an enormous bureaucracy, and in the countryside I often saw evidence of enforcement. [...] Han Chinese are limited to one child; urban Mongolian residents can have two; and rural Mongolians are allowed to have three. (p.191)
* It's illegal for a Chinese doctor to tell a pregnant woman the gender of her child, but bribes are common (p.192)
* in China, there is no national health insurance, and city residents usually rely on their work units for coverage. Most farmers are completely on their own, which is one reason they tend to be so careful with their saving. It wasn't until 2009 that the central government began to take steps toward establishing some form of universal health care... (p.211)
* Chinese love to talk about food. Most Chinese are naturally so social that if you throw them together they talk endlessly, even in the most stressful situation. (p.217)
* all tobacco companies are state-owned ! From the government's perspective, smoking is important to stability, both economic and social. SOme cigarettes are even subsidized. ... The status is far more addictive than the nicotine. (p.294)
* Because chinese farmers can't use their land as collateral, they need village backing for any loan application (p.241)
* according to traditional Chinese medical beliefs, it's bad to put anything cold in your stomach. (p.271)
* In Sancha, villagers reused almost everything and they rarely ate packaged food. When it changed, the best proof was the garbages that were everywhere. It wasn't until 2006 that the county finally instituted regular garbage truck service. (p.314)
* in 5 years, the per capita income in Sancha tripled (p.317)
* the Chinese have countless obscure beliefs about which times of day are bad for fluids, and the end result is that most people simply don't drink much. (p.330)

### About children

* in certain regions, the planned birth policy hadn't been strictly enforced. People just pay a fine and have more kids (p.117) 
* Twins represent a kind of lottery prize - for most people in China, that's the only legal way to have two sons (p.164)
* in the countryside, traditional parents avoid flattery... Chinese superstition that pride attracts misfortune. (p.180)
* there is no concept of discipline with regard to consumption. In the recent past the village had been so poor that people ate whenever they could and a parent's main resopnsibility was to feed a child as much as possible. Then, everything changed so fast that people couldn't adjust: a child eating was always a good thing, and there was no point in having a new television if you didn't use it. (p.307)


### About education

* in a classroom, the group is the foundation for every endeavor, and each child always knows his place within that organization. There is the notion of _Peer discipline_: children who misbehave are often asked to stand before the class, where other students help the teacher criticize the guilty party (p.250)
* parent-teacher conference are communal meetings: all the parents are there and listen as the teacher summarizes each student's performance. There is no greater loss of face than hearing in public that your child does poorly at school. (p.251)
* the _Elementary School Rules of Daily Behavior_: "Be interested in national events, respect the national flag, ...", "cherish the honor of the group"... In the entire list, only one was directly academic. The word _bu_, do not, was used 28x (p.274)
* people sincerely care, and their faith in learning runs deep. Despite low pay, teachers tend to be dedicated and parents try to do their part. Even the poorest people have faith in books. (p.276)
* everything stills revolves around memorization and repetition. Rarely was any subject personalized or contextualized; the world devolved into statistics and numbers and facts. (p.278)
* history lessons were narrowly aimed at proving the greatness of the Communist Party (p.278)
* self-criticism is really important and children learn it rapidly.
* "Sometimes it depressed me, but I had to admit that the education was extremely functional. Wei Jia wasn't necessarily leaerning the skills that I valued, but there was not question that he was being prepared for Chinese society". (p.280)

--------------------------------

## Third part: factories

### Miscellaneous 

* among uneducated female migrants, jobs tend to be sharply divided according to looks. A pretty woman is more likely to find work in a barbershop or as a restaurant hostess; the plainer girls end up as waitresses or factory workers. Jobs are easier for women who are good looking, but there are also pitfalls (prostitution). (p.113) 
* Chinese work world, looks matter greatly, especially for jobs with little educational requirement. (p.168)
* smart Chinese women with little education often become accountants or secretaries, and from these posittions they can rise in the factory world. But uneducated men have fewer alternatives to the assembly line. (p.168)
* Around Zhejiang, it is full of one-product towns (p.357)
* the _Wenzhou model_: the business strategy couldn't have been simpler: low investment, low-quality products, low profit margins. Low education too. (p.365)
* there's no shame in being a cold-blooded entrepreneur (p.367)
* "now everything is already being made by somebody in China. No matter what you make, you are going to have competition. So now it's not the product that counts. It's the volume" (p.373)
* The story of _Liu Hongwei_, the guy who memorized a machine and sold the blueprints to several companies afterwards.
* The pleather factory.
* According to Boss Wang, the ideal worker is a young, with little education and inexperienced woman. Not too pretty. (p.395)
* Women are paid less, a detail that was noted openly in listings, along with regional and height preferences (p.397)
* China doesn't have a nationwide standard, because of wide regional disparities in wealth, each city sets itw own minimum wage. (p.400)
* Most workers care about hours as much as they care about the starting wage. Most people want to work as much as possible - since the're away from home, there's no point in having free weekends or holidays. (p.401)
* initiative mattered most.
* In a chinese boomtown, it's all business: the free market shapes all early stages of growth, which is why entertainment options appear instantly but social organizations are rare (p.446)
* when people negotiate with bosses, they find any advantage they can, and I understood the value of a nonexistent child. (p.468)
* we can't predict the snap decisions of a boss. People rarely planned carefully, especially in a place like Zhejiang (p.485)
* in the development zone, factories received tax breaks for their first three years of  residence (p.501)
* 



### About guanxi

* _guanxi_, "connections". A businessman learns to _la guanxi_. Literally "to pull, to drag, to haul" and the description is apt: it takes work (p.230) nothing captures the texture of _guanxi_ better than cigarettes... every gesture with a cigarette means something. (p.292)
* p. 426: gifts are standardized and portable, which makes them a kind of currency.
* Guanxi is logical: an official receives a gift, a factory receives favorable treatment. (p.430)

### About cities economy

Cities have relatively little money coming from the central government. They also can't charge significant property taxes, because land is still nationalized. The tak base is weak; it is impossible for a city to survive on its tax revenue. Yet, Chinese cities spent money everywhere.

The profit comes from the constructions. In the countryside, all land is collective [...] the city can acquire the land at will, and they pay set prices that have been established by the government. After the sale is made, the city can build basic infrastructure and reclassify the region as urban. And urban land-use rights can be auctioned off at market rates. Buying rural land and reselling it as urban can be practiced only by governments from the township level up. The profit from such exchanges are immense. So if a city hopes to stay solvent, it must continually expand. 

By 2006, the central government had realized the risks of this system. But authority had become so decentralized that rules were hard to enforce.

See pages 431-432.





TODO: mongols p. 99
