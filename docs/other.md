## Labor law

* If no written contract is signed by the “one-month” deadline, the employer has to pay double salary to the employee,
* The Chinese work week is to be no more than 44 hours long, and employees must have at least one day per week off. All workers have Chinese national holidays off. 
* Laws set a maximum limit for overtime work (3 hours/day and 36 hours/month)
* Minimum wage in China varies from province to province
* Women are restricted from doing certain types of heavy work, including work that is underground or some types of work high up in the air, and there are regulations regarding menstrual cycles, pregnancy and maternity leave. Otherwise, they are to be given equal work and equal pay.
* Part-time workers can be dismissed at will and without compensation, which is not allowed with the termination of full-time employees.
* 