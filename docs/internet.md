## About Wi-Fi

I discovered how hard it is to connect to the internet on my first stay in China. I was staying in the Holiday Inn hotel in Shenzhen. The first night, everything was fine: I had access to google, whatsapp, telegram, everything. But every time i changed floor or took the lift, I had to switch the Wi-Fi off and on to get the connection working again. And besides this, every connection is very slow.
The second day, Whatsapp was not available. Then it came back sporadically. Tumblr was also on and off. Google drive was working at such a slow pace that it was unusable without VPN.

First, it is important to stress that the government controls and censors public Wi-Fis quite heavily and that their policies might change overnight:

> When it comes to public access to the Internet, the government's controls can make the experience exceptionally frustrating for foreign travelers. Quite apart from direct censorship (see below), other blocked services to which sites commonly link can lead to slow page loading, and the regulations imposed upon establishments offering a connection may change overnight, necessitating them to change their approach, or even abolishing access altogether. 
> [...]
> In China, Internet access is prone to rapid change due to the imposition of new regulations, and it seems highly unlikely this state of flux will change anytime soon.
> ((source)[https://www.chinahighlights.com/travelguide/article-internet-access-in-china.htm]){: target="_blank"}

China also restricts certain websites, especially those that allow free interaction between people. This means no Twitter, Facebook, YouTube, etc. Instead, chinese use their own copied versions of those sites (btw some copies are even better than the original, see this [TED talk](https://www.ted.com/talks/michael_anti_behind_the_great_firewall_of_china){: target="_blank"})

I heard that Google Play is also blocked in China, but personally I was able to install Cisco VPN without any trouble from Shenzhen.


When China kicked Google out, some VPN companies had concerns. The Government told them there was not problem, as long as all the products stay in English only.