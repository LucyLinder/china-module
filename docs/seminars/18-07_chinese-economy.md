The PDF if available [here](../resources/pdfs/18-07-chinese-economy-shenzhen.pdf){: target="_blank" }
{: .alert-info }

__Chinese economy and Shenzhen special economic zone: the transformation of China__

Dr. Guo Wanda, Executive vice president, China Development Institute (CDI). email: guowd@cdi.org.cn.

think tank - consultant for public policy making and central government. It is different from a business institute. 

Chinese name: China central government institute (??).

In China, think tanks are different from the western ones: one of the medium medium. In the south, only one. Areas of interests:
- relationships between Shenzhen and Hong Kong
- ...


## Chinese economy in the recent past

Three big leaders:

* Chiang Kai Shek: 1942 and 1955, made the cover of _time_
* Chairman Mao: made the cover of _time_ in 1949 and 1957; when he took the power, for the cultural revolution.
* Teng Hsiao-ping, cover of the _time_ in 1979 and 1985. When he took the power and started reforms for modern transformation of China and ??

After the 1949 foundation of the People's Republic of China:

* _first five years (1953-1957_: a new economic order modeled on the Soviet Union example, emphasizing the development of capital-intensive heavy industry
* _great leap forward 1958-_
* ...

Before 1978, a completely socialist country. The government is allocating resources, there are production quotas and the goal is quantity rather than quality. Investment in heavy industry, many famines and shortages, etc.

After 1992, the market becomes the basic means of allocating resources. The reform introduces competition, "free market", etc.

Planned economy vs market economy:

* resources allocated by government vs market
* government vs enterprises decide what how many and hot to produce
* price is subject to the plan vs to market supply and demand

China's economic reforms:

- _rural reform_: increase of agricultural production and rural income. Before, farmers couldn't move to the city or produce what they wanted...
- _open door policy_: turnaround from an inward-looking, self-reliant enconomy to one tha participates in the world economy (foreigh trade, FDI, and SEZs)
- _industrial reform_: enterprises and prices reform, first tests with the special economic zones, etc.
- _financial reform_: company's responsible for financial performances and borrow money from banks; raise capital at the stock market
- _SEOs reforms_: ownership restructuring

Always step-by-step reforms: first applied to pilot areas, then applied to the whole country if successful. Usually: coastal area -> riverbank area -> border area -> inland area -> whole country.

WTO (World Trade Organisation): if not part of it, you are left aside the world economy. China is a member only since _11th December, 2011_.

__results__:

The result of reforms and such: a GDP growth of more than 8% for 30 years (1978-2008). We are usually at 0.5%, even negative during crises. So not bad ! 

Another result is the massive urbanisation: in 2015, urban population became higher than rural population for the first time. In the planned economy, about 20% lived in cities and the number was very stable. Each year since the reform, about 1% increase of the urban population (1% is more than 10 million people). 
Urbanization will be the driving force in the next 10 years. We were at about 50% in 2010, so there are still some good years to go. 

Chinese industrialisation -> China as a World Factory. Labour forces very cheap (rural migrants); the real estate prices are going up, but it was very cheap in the beginning (note: in China, you don't buy land, you rent it).

__SUMMARY__: three transitions after the reform:

1. from a planed economy to a market-based economy
2. from a rural, agricultural society to an urban, industrial one
3. from a non-WTO nation to a WTO one

## 

Will China be a rich country in the future ? Two views:

* from the economic point of view, il will be a rich country
* from the political view, China will be a poor country. Rand Corporation (2009): it is still not a legal society; the education system has lots of problems, corruption is important, etc.

Main challenges:

1. _Aging population_: the peak will be in 2030, with an aging rate of 30%.
2. _water shortage_: also a climax in 2030. China supports 21% of the world population with 7% of the world's water resource. And water consumption is 5% higher than the average...
3. _Carbon emission _

Drives for the next 10-20 years:

* huge domestic demands: urbanization, internet, etc.
* advantage of cheap labor: labor is not as cheap is it was, but the increase is slow, so it will still be interesting in the next years. And we are not only talking about the general workers, but also engineers and graduate students. Since labor is cheap, it is possible to reinvest in Innovation, which will be the most important thing to keep things going in China
* local officials still have great motivation to develop the economy. This is a chinese characteristic, because they have local taxes so it is very interesting to develop the region -> investment
* the motivation to get a better life -> a new kind of demand that drives the Chinese market

Until 2008, >8% of GDP growth. Since 2009, more in the "decline"; the grd growth is decreasing, reaching a "new normal economy" at about 6-7%. This is likely to be the normal growth for the next years to come. But for that, they need to transfer capital into the labor to capital in the innovation (cheap labor will be more rare)

In the past, the FDI (foreigh capital investment) was always way higher than the ODI (Outward Foreigh Direct Investment). FDI and ODI reached the same amount in 2014. _In the __New Normal Economy__, ODI gradually overtakes FDI_.

One problem: if investments are only based on labor or land incentives, then companies might go away or stop investing altogether because both will become more and more expensive.

__the B&R__: the Belt and Road Initiative. Unlike FTAs (Free Trade Agreements), _B&R_ is an open initiative with no thresholds. 
USA is not in this belt... How was the reaction ? Obama was not happy, but Trump is open to the B&R.


Before, many foreign companies were just producing in China and exported to other markets. Now, more and more companies are targeting the Chinese market directly, since it is developing very fast.

## Shenzhen: China's most successful SEZ

__SEZ__ stands for _Special Economic Zone_.

Shenzhen is the most dynamic metropois, the GDP grew by over 25% in the last 20 years. Still at 9% in the last 5 years. This is one of the zone were economic reforms are tested. It became an SEZ in May, 1980. 
The average is 33 years old, so a very young city. 

Shenzhen is very close to Hong Kong and every day, many people are flooding between the two cities --> "_the twin city_".
_Hong Kong stores in front, Shenzhen factories in the back_. This is a very successful business model, but it has loosened over the years.
The rise of Shenzhen could not have happened without Hong Kong: HK is the teacher for chinese reforms. The land reform for example.
Plan: "_Qianhai & Shekou Free Trade Zone_" is new test for opening up.
SEZs as a Opening-Up Window of China, are mostly foreign-funded; they have more economic autonomy and a lot of favorable policies for investors in terms of land, tax, custom clearnace, finance, logistics, etc.
Shenzhen has about 4% innovation (the US is at about 2.2%), very high !
92% of the enterprises are private, creating 80% of employment and 40% of GDP. In most Chinese cities, government-owned enterprises are still common.
Innovation plan for the region: manufacturing -> R&D, OEM -> ODM, more productive sector, an updade of the traditional market (distribution format).


SEZs are mostly coastal areas with ports; export oriented.

Three types of SEZ coexist in Shenzhen: tax-protected zones and free trade zones, high-tech industrial parks and industrial clusters.

In Guangdong, three free trade zones, each with different functions. In Shenzhen Qianhai % Shekou: financial district, bonded port area and business.

In July 1rst, "_Framework Agreement on Deepening Gunagdong - HK - Macau Cooperation in the Development of Bay Area_". What it means: there will be more special poiicies to develop the region; more connectivity with new infrastructures (new bridge in 2017, new port in 2018, new speed rail in 2025) and possibilities (accepting the octopus card in Shenzhen).

The real changes will come from or depend on:

* the education
* the move from labor incentives to a more skilled/innovation oriented industry

These are area were China could learn from Switzerland.

One of the real strength of China is the power of the mass: for production, but also the inner market that is one of the biggest in the world.

For him, China still needs 20 years to catch up... But do it really ?
{: .alert-warning }
