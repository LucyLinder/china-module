Visit at the Chinese ambassy in Bern.

## Discourse of the Ambassador

_Notes_: These are part of the Ambassador's discourse. Some sentences have been slightly modified, but the meaning hopefully stayed the same. In my opinion, the main points were (not in order): 

1. the Chinses economy is huge and is a great opportunity for us, we are welcome there;
2. education could be better and the Swiss model is a great source of inspiration;
3.  China is not a developing country: it is a developed one. 
4.  Switzerland and China have a real frienship that comes all the way from the first declaration of the PRC;
5.  Young Chinese are now better educated, know English and are very active/vivid, even more than us.


---------------------------------------

When I was 20, it was my first time taking a train, my first flight was in my thirties. My first experience outside of China was in 1988 and it was in Zurich. My first impression was that we were 50 years late. 

When we talk about China, everything has been done in the last 40 years. It is not a developing country, it is a developed one. 

PRC was established in 1949 and it was in 1950 that Switzerland acknowledged the PRC. But since everything takes time in Switzerland, Sweden became the first one to officially recognize PCR and Switzerland became the second.

I just want to explain how close our two countries are: when you are always thinking about your girlfriend/boyfriend even when you are not seeing each other in a while, this is how close we are. Even if Switzerland and China may have divergence of opinions, it is like between good friends with different points of view.

There is a line from Zurich to Beijing and vice-versa. Even with one or two flights a day, it is not enough to keep up with the demand and it is always full. So a new line has been opened and more flights are coming.

There are a lot of night-life activities there. When I mean night-life activities, don't think too much: I am talking about restaurants, and such. And we are also good at drinking, so be careful. When you arrive in China you will see how developed we are.

When you arrive in China and see Chinese young people, their English may be much more fluent than yours. They are much more active and very gifted in making friend, so they may try to become your girlfriend or boyfriend. We have beautiful boys and girls, you are beautiful boys and girls, so be careful.

In the last forty years of reform, we have achieved quite impressive things in term of economy, we are now the second economy in the world. But there are still many things to do in term of education especially in engineering. 

We just signed a contract about exchange and education.Please advocate the Swiss education in China [...] I would like to promote the Swiss education more and always speak highly of it. I always want more Chinese to come here in Switzerland to receive the real knowledge and eduction carried out from here. So if a Chinese ask you to become your girlfriend, you should answer "come here and I will be friend with you".

There are about a thousand Chinese in Switerzland, only about seventy in the US.

The doors of China will always be opened for you. We are eager for talent from all around the world. Of the top ten enterprises in CH, all of them have a branch there in China. They make more than 30 billion US dollars of profit each year there.


## Chinese Buffet

We had the opportunity to tast more than height different typical dishes, along with a very good red wine and other non-alcoholic drinks. The reception was great, with at least one Chinese employee per table. At our table, the young man answered many of our questions with eagerness; he was very opened. 

One of the most interesting answer he gave us was when we asked what was the main thing they could learn from Germany or Switzerland. He astonished us by talking about politics. In his words, "_we could learn more about the voting system. Here, you are accustomed to vote more than four times a year, that's normal. We don't have that in China (but I think we should)_".
