# Company presentation

The PDFs are available [here](../resources/pdfs/companies/12-07_GF.pdf){: target="_blank"}

The speaker is responsible for Asian sales companies. Germans always have questions, so two people for Europe and one for America.

## Introduction and figures

"We are industrial pioneers". It was a family-owned company, Fischer is the family name.

* founded in 1802
* headquarters in Switzerland
* 14'808 employees worldwide, 3'100 in the Machining Solution 
* present in 33 countries
* 131 companies, service and sales centers
* 51 production plants
* 38 centers of competence: important for clients to have somewhere to go to get information and showcases
* sales of CHF 3.75 billion, 916 million for the Machining Solutions division
* margin of 8.3% in 2016, ROIC of 19.3%


In 2018, they will move near the Manau factory because they are out of space.

Three divisions:

1. GF Piping Systems
2. GF Automotive
3. GF Machining Solutions


## Machining Solutions

* located in Nidau; 
* mostly milling machines 
* They have exactly the same in China: quality systems and strategies/visions have been transfered there. 
* For milling, mostly assemblies: they get parts from elsewhere and only assemble the machines. 
* Spindling are made (SpecTech). 
* Tooling & automation: milling machines for example are made to run 24/7, so automation is a key component. At the end of the day, they are providing the whole solution: not just the machines, but the whole stuff (line, process, automation, etc.). 
* Customer services are also a key feature (see competence centers): applications are also part of the picture. This is a way to make a difference compared to the competition and to justify the prices (which are kind of high).
* Many technology labels (Mikron, AgieCharmilles, etc. that are quality signs and mostly purchased brands), so the +GF+ logo is always at the center ^^.
* Microlution is the latest company they purchased. Liechti is a small company active in aerospace. It is "independant" but part of the family. 

## Sales and sales companies

Sales, 2016:

* 916 millions total
* 232 million for customer services

Worldwide sales. Making machines then selling them to sales companies that sell machines to the clients. They are the ones taking care of shipments and installations. If some modifications are needed for the country, done by them as well. The technical department of +GF+ never sells directly to clients. Sales companies have a contract with +GF+ (so part of +GF+ in a way).

## About China 

EDM in Nidau and Beijing, Milling in Nidau and Changzhou. 

> In CH, we have many customers and one or two machines per customers. In China this is the opposite: not so many clients, but many machines per sale (40 or more). 

There was a difference in quality between Switzerland and China in the past, but now it is has been equalized (many expers sent there). But with the "_made in china_" label, people expect the price to be lower.

Proving the quality of the made in china machines was not that easy. They often want Swiss-made. Machines are always developed here, then transfered to China (they don't have enough engineers).

> Not all the machines must be the best of the best. The quality is not the same, but it is good enough.

Korea market completely different from Japan, you need people there witht the Swiss attitude. In general, Asia does business very differently.

> Quite a lot of machines made in China come in Europe and vice-versa. In Germany for example, they need machines that do the job and at a lower price, so they buy chinese. Many chinese companies want the best of the best, so they buy Swiss-made. 

HEM: High Efficiency Machines mainly done in China ?

## Miscellaneous

There is a so-called academy here, engineers need to come to Switzerland at least once or twice a year to be formed as experts.

TechPoints are dedicated to some segments: they don't have the whole portfolio but just some machines.

## Market segments

The main segments are:

* Automotive. Milling, for example the machines for engraving tires or the symbols on the "boite e vitesse". Still mechanical, but trying to use laser instead. Takes year to introduce a new technology like this. Lasers to make the surface of the dashboard look like leather, etc.
* ICT/Electronics: tooling for mobile and tables; etc. Connectors, semi-conductor molds, etc. Only one micron deviation is tolerated, so you need precision !
* Aerospace: this market is explosing. Many new projects in China, they are now making planes by themselves. In aerospace, once you approved a process, it won't change, at least not for a long while. It takes many years for a new technology/process to be approved and fully tested.
* Medical: machines for dental and equipment. 
* Energy
* Research: industry 4.0 on board from the beginning.

R&D: meetings with professionals from all segments once or twice a year to exchange and decide where to put money (?). The resaearcu and new models are developed in Geneva, all centralized there.

## The machines

Kinematic five-axes for milling, core-cooling spindles to decrease the amount of variation of the temperature.

Wire-cutting EDM usng lasers.

laser texturing: engraving, micro structuring, marking, labelling. The most difficult part is the designing/programming of the lasers.

additive manufacturing: basically 3D printing with stainless steel (sand, layer-by-layer). They started to ship software as well to compute the cooling time and stuff, because it requires a lot of knowledge.


Automation: putting machines together in one automated line. They provide the management software for that.

# The visit

* here, mainly assembly. Main parts come from the outside, mainly Germany and neighbors. Why ? Because if there is a problem on a piece from Romania, it is much more complicated to setup a new shipment. From Germany, easier.
* pre-assembly: every square of work has everything (hammers, etc.) and also a machine for testing the piece. Early detection of problems, each sub-part is tested
* about 400 machines per year on this site
* assembly: one machine = 5h at each spot. About 4h hours of assembly and 1 hour for testing. Testing is done by another _monteur_ mostly a visual test. In the end, final machines are tested by people from the bureaus. 
* since 5h per spot, after they move to the other building, they will work 10 hours per day four days a week with small teams (40h/week). Also best because many tests take about three days to run.
* they try to be the best at the mechanical level, but there is always a software compensation needed
* they have a place with cards and board for quality management and the following of products on the lines: Quality boards, Q-cards movements, PEA boards, Q-audit. I also had a glimpse at a kanban karte. There are traces of those cards on informatic supports and reports made during the reunions in the morning, but informatic is not at the center of the processes.
* all their advertisements have a high girl-to-men ratio, but I haven't seen any girl here yet. 


__about SCM__:

Little small pieces: all are inside little drawers with a buton on the side. Employees can take whatever they need and if they see the stock is running low, they press a button: the command and recharge is automatic. There is a bill at the end of the month, but no detailed trace of every stock.  Different threshold for each piece; employees assess the state of the stock "au pif". No gestion of stock in the ERP per se. We know there are pieces of type X, not exactly how many.
--> trust in the emloyees and suppliers (for small pieces, only one suppliers)
 
 2 machines per day, no or little buffering (pas de "machines tampons", donc il faut que ça suive). A short-term vision of about 4 days. Most machines are customized and cut for the client, so machines are assembled mainly on demand.
--> lean manufacturing

They have an ERP and an SAP, but very standard; basic functionalities, no customisation whatsoever.

For the storing, one robot connected to the SAP. Someone scans or enters a command number and the robot will put it or get it from its place on the huge _étagères_. For smaller pieces, they use a standard rotative system.

No special software for business and logistics, but special softwares for machine management (so in the product itself).

Sales companies are part of the GF family, but isolated from the technical center. Sales companies take care of the clients, do the commands, organise deliveries and _service après-vente_. The technical factories only assemble machines (parts from different suppliers, only very special parts that need to be perfect are done by GF itself). The packaging (putting machines into boxes for transport) and shipping is organised and taken care of by other companies (in case of the boxing) or the sales companies.

The technical division sometimes deals directly with clients (about 10-20% of the time) or get directly involved, since some clients have very special needs and demands. Other than that, clients don't deal with the factory directly.

![delivery channel in +GF+](../resources/GF-logistics.png)

# +GF+ milling fundamentals

One of the main problems is the heat.

HPC = High Performance Cutting, HSC = High Speed Cutting.

From most basic to best:
1. HEM, high efficient machining created in Changzhou
2. HPM: high performance machining: removed material rate is the most important, very good roughing, not so good finishing surface quality.
3. HSM: high speed machining: surface removing rate the the most important. For pieces that require more rotations and surface _balayage_. Very good finishing surface quality.
4. mu: micro machining technology. Surface roughness and the achivable part accuracy is the most important. Pieces of money, eletronic components, micro engravings, etc.

__vocabulary__:

* _roughing_: faire des pièces rapidement, genre prototyping. Souvent, besoin d'enlever beaucoup de matière, mais les finitions ne sont pas importantes
* _milling_: fraisage
* _drilling_: percer
* _speed in milling_: plus on va vite, plus la surface sera nette et donc plus la finition est intéressante, lisse. Effet miroir possible qu'à grande vitesse.

En gros, vitesse quand on veut une bonne finition, roughing et HEM quand on a beaucoup de matière à enlever mais que la finition n'est pas le premier critère.

_What is industry 4.0 for you ?_

Better accuracy and communication from the machine to the client. More exchanges with the suppliers, integrate more sense in the machines (cams, sensors, etc.) 


----------------

https://www.daviesrobson.co.uk/clients/georg-fischer/ 

Georg Fischer (GF) Piping Systems has a turnover of almost £1bn and is one of three divisions of the Swiss based Georg Fischer Group.

As the business profile has changed, with customers demanding more and more products with shorter and shorter lead times, the operating practices within their NDC in Coventry were being stretched.  Our supply chain consultancy was invited to undertake an Operating Review that included an assessment of the supply and demand profiles within the warehouse and the existing workload throughout the working day involving the planned and actual manpower and the usage of the high bay equipment across the three shifts, as well as the utilisation of the available picking, packing, sortation and generic work areas.

Since the implementation of key recommendations, such as the business processes and the two-shift system, the productivity and performance of the warehouse is steadily improving.