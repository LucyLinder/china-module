
## About Huawei

Huawei is a leading global information and communications technology (ICT) solutions provider. Products and services are used in more than 170 countries and regions, serving over one-third of the world's population. It counts about 180'000 employees.

Different targets and partners:

* _Network carriers_: together with telecom carriers, Huawei has built over 1'500 networks. 
* _enterprises_: open cloud solutions and agile networks for enterprisew, activities in domains such as Safe City, finance, transportation, and energy.
* _customers_: handsets, smartphones, tablets and laptops

Huawei is an active member of over 360 standards organizations, industry alliances, and open source communities. They have submitted over 49'000 proposals to drive standardization and joined forces with industry partners to innovate in emerging domains like cloud computing, software-defined networking (SDN), network functions virtualization (NFV), and 5G. 

Huawei implements the rotating CEO system under the BOD's leadership.
Huawei experienced a 32% increase in revenue growth in 2016
Huawei is one of the world's largest patent holders and investors in R&D: more than 10% revenues are injected into R&D; As of 2017, Huawei had filed 57'632 patent applications in China and 39'613 outside China, with a total of 62'519 patents granted --> In 2016, they ranked second in terms of invention patents in China, 7th in Europe, and 25th in the US.

----------------------------------
"__Made in China 2025__" is an initiative to comprehensively upgrade Chinese industry. One of its core components is green manufacturing, carried out through special project initiatives and the implementation of green manufacturing projects. Green manufacturing is one of China's "__Five Major Development Concepts__" (Innovation, Coordination, Green, Openness, Sharing) that are conducive to the coordinated development of industry and the natural environment.

In 2016, Huawei Device (Dongguan) Co., Ltd. was approved for the pilot project "Extended Producer Responsibility Pilot for Electrical and Electronic Product Manufacturers", an initiative of the Chinese government. It participates in the "Ecological (Green) Design Pilot" initiative of China's Ministry of Industry and Information Technology.

Huawei is services, not industry. Are copied now, not copying.

-------------------------------------------

## The showroom

* smart city with IoT devices and a central control center. The presentation is amazing: you can track traffic, police and ambulance cars, etc. They even made a VR application with an HTC Vive and told us the technology was already deployed in real life. But in fact, they are just at the testing phase and it is finally not big deal.
* trash cans with sensors and level detection (not so different of what we have here)
* fully-integrated city lights with motion detection, cameras, sensors for the environment (CO2, etc.)
* face detection. For privacy, it seems that they provide cloud solutions but mostly private ones: they install them on premises, don't keep the data in their Data Centers
* desktop cloud: little appliance directly connected to PaaS, possibility to upgrade the VM capacity anytime.
* Omni-channel banking: sort of e-banking device which looks like an ATM. You can identify yourself and them ask question, open or close accounts, etc. 
* AVG - Auto Guided Vehicle and smart logistics solutions

Concerning the IoT technology, they are currently negotiating with other big companies worldwide to agree with a common standard. _3GPP_ has good chances.  

## Corporate presentation Huawei Switzerland (notes)

* 180'000 employees, 45% of which are R&D engineers
* this year, is amongst the top 80 companies worldwide
* many activities in India because are very good developers
* 45 training centers around the world
* 75 billion USD sales revenues in 2016
* 18 local R&D sites throughout 8 countries
* in 2016, 45.3% of revenues in China, 30% in EMEA (Europe, Middle East and Africa)
* operates in 3 different boxes, the last two are fairly new (2010):
    - technology solutions to carriers companies around the world: they are the ones having deployed the more 4G in the world for example
    - consumer division: handsets (right after samsung and another one), laptops, sportswear equipment, etc.
    - enterprise division: deployment of data centers and private clouds, big data analytics (for energy companies for example or financial analytics (fraud detection)); wifi networks in stadiums and companies (German stadiums for example); teleconferences technologies for government, high speed networks for universities and hospitals
* currently trying to make links with private and public companies to foster the future: smart cities, IoT networks and sensors, etc.
* One of the key point: you need to collaborate internationally. 

Long-term investment in innovation is a key strategic decision. They ensure at least 10% of the revenues are reinvested into R&D. In 2015, they invested 15.1% ($9.2 billion US) and a total R&D investment in the past decade (US $37.1 billion).

They are N°1 Chinese company with the largest number of patents in China, one of the top 50 patent holders in the US, one of the Top 10 patent holders in Europe. In 2015, made the top 100 global innovators (by thomson reuters) and were part of the most innovative companies (by BCG).

Many technology partnerships. Save for the future: cooperation with European technical scholars to show them China and learn issues of today and tomorrow. 

80% of the waste are reused. Most of the transports are made by sea, then by air; C02 standards to ensure best practices in the field of sustainability + agreement with suppliers to ensure they respect some kind of sustainability guidelines.

## In Switzerland

~300 employees. Swisscom is the first European telecom operator to have launched the G.ast commercially with Huawei. Huawei developed specific solution for the Swiss market/topography: FTTS with fiber and copper for the rural areas where FTTH would e too expensive to rollout.

Sunrise selected Huawei as new SINGLE technology partner for network  renewal and operations. Large network improvement program. 

They are also building their brand: red trams in Zurich and Bern, co-designs with Leica for cameras on handsets, sponsoring of Kloten Flyers. So cooperation with public or private companies. Also sponsored a lot of different activities in Switzerland.

They became an active member of the Swiss ICT family. Among other things, they created a "_Seeds for the future program_" to nurture more ICT talents: program for Swiss students with visit to China.

Got the "_Best Chinese Investor_" Award in Switzerland.


## Q&A

_Main reason of success ?__ 

In the DNA of the company is the consumer. They spend much time with the customer to understand their needs. Commitment to continuous learning: IBM in 1996-1999 gave advice, as well as the A-group for HR.  They know they need advice and don't fear to ask for help outside the company.


2010: consumer + enterprise divisions are added. They are 42% of the revenue in 2016, so it was quite a good decision.

The commitment to invest in research and basic science is clearly a strong factor as well as partnerships with other ICT companies.

_depending on the Chinese government ?_

The story of Huawei is also a story of Shenzhen SEZ and the help of the government to help private companies emerge. Even if they are private, they have been helped by the taxes exceptions and stuff. 

US and Huawei were not in good terms, but it became better in the past years. Still not doing business with 4G/5G in the US.

> If you want the best product, you need to collaborate with the best wherever they are. Talent has no border, even if governments does not always agree with it.  

_easy to find good engineers in China?_

China: 5 years government plans periods. One of the policies in the current plan is to ... ??.

China is moving up very quickly, becoming a real innovator. The government is commited to improve the innovation and added-value here in China. 
 
_sustainability?_

New mobiles will consume 80-85% less than before. Sustainability and efficiency is in all the products. 

_sustainability contracts_ need to be signed by suppliers and controls are done by Huawei to ensure that they follow the sustainability best practices.


## My impressions

We didn't learn much, because the discourse is well greased and completely standardized: the show room, the presentation, the answers, nothing is transparent.

It was interesting, but you could sense there was a lot of bragging and faking. 

In official documents, they are environment-friendly. If something goes wrong, it is the fault of the suppliers, never them. Many fake declarations and consealed stuffs. 