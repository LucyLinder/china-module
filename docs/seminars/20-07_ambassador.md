The PDF if available [here](../resources/pdfs/20-07-consul-Dongguan.pdf){: target="_blank" }
{: .alert-info }

Presentation from the "_consul général_".


* Is Guangdong was a country, would be the 15th. Before Switzeraland ! 11% of China economic input.
* China never looked at us from above because of our size. Ìn Switzerland, about 1.5 millions Chinese staying overnight (in hotels) per year.

Switzerland is the first for many things regarding China:

* Schindler was the first joint venture between china and a foreign country 
* One of the first to recognize China as a market economy (some countries still don't, e.g. Japan). 
* The biggest trade partner after EU and USA. Signed a free trade agreement, the first to do so in the so-called Group of Twenty.


1979, death of Mao, economic reform. Guangdong was rural. 4 SEZs created. The location was strategic based on neighboring countries/regions (Hong Kong, Manau). But Shanzhou, why? Many don't know. The consul asked the last time he was there and the answer was because there were a lot of rich people that moved abroad, so they hoped it would bring them back. Now, 5 economic zones, 3 in this province. SEZs have incentives from the government such as taxes, land, etc.


17 years for the Gothard, 9 years for the Hong Kong Zuhai Macau Bridge:

<iframe width="400" height="225" src="https://www.youtube.com/embed/Kikg1wlLieQ" frameborder="0" allowfullscreen></iframe>

(see also [this video](http://www.hzmb.hk/eng/video_centre_b_02.html){: target="_blank"})

Raw material import from Switzerland dropped in Guanddong the past years. A suggestion is that this area is more and more about services and innovation, so imports mostly go to other provinces still completely into the secondary tier ? 

_Innovation: about CH and China_
Switzerland is a Land-lock country with no resource. 100 years ago, we exported mercenaries. Now, we focus on the know-how: it was innovation and brain or nothing. We are all the time innovating. We have this innovative mind, as well as patents and know how. 

Huawei and tencent are now better than many western companies, most employees are very young. Chinese companies buying Swiss ones, not to destroy them (as other western countries do). No, they want the made in China, the swissness. They won't transfer hq to China.

Chinese Studies: you don't question your professor. You listen, pass the test. Interaction during lessons doesn't exist.
