The pdf is available [here](resources/pdfs/11-07_01-introduction-chinese-culture-language.pdf)
{: .alert-info }

## Language

## Some examples

* To present yourself, use `Wǒ shì Lucy`, pronounced `wa shoue` and meaning _I am Lucy_ (see [this video](https://www.youtube.com/watch?v=BH6msZGUIyA){: target="_blank"}).
* To say hello: `ni hao | nĭ hăo`
* Thank you `xièxie` (pronounced `shie shie`)
* I don't understand: `Wǒ bù dǒng` (`wo bou dong`)
* You're welcome: `Bù kèqì` (`bou queishi`)
* I don't want: `bù yào` (`bou yao`)

More language examples:

* at the market:
    - [https://www.youtube.com/wa tch?v=7lVXFWjJIjM](https://www.youtube.com/wa tch?v=7lVXFWjJIjM){: target="_blank"}
    - [http://edition.cnn.com/2011/10/21/business /china-business-investors-culture](http://edition.cnn.com/2011/10/21/business /china-business-investors-culture){: target="_blank"}
* at the restaurant and hotel:
    - [https://www.youtube.com/watch?v=4Fmfx-CftJI](https://www.youtube.com/watch?v=4Fmfx-CftJI){: target="_blank"}

Chinese language is logic, built from simple blocks and an easy grammar.

mobile phone is ` shŏujī`, which is constituted of two symbols: hand and machine.

### Tones and words

Young chinese usually start by learning how to pronounce characters. The _Hanyu Pinyin_ is the most widely used Mandarin romanization system. Most words are just made of a _consonant_ and a _final_. 21 consonant, 27 finals, so about _567 words_ possible. In modern Chinese, many nouns, verbs and adjectives are disyllabic (composed of two syllables).

How is it possible ? The __tone__ used will change the meaning. The tones differ in height, length and intensity. With tones, you get 1'300 words!

![chinese tone example](https://www.misspandachinese.com//wp-content/uploads/2011/04/Tones-in-Mandarin-Chinese.png){: style="max-width: 500px"}

The difficulty arises when you have many words with different tones in the same sentence !

Note that tone is not written, so you need to guess the right meaning of the word using the context, the word order and some tricks.

### A “bite” of Chinese grammar 

* Chinese has very few grammatical inflections;
* To indicate past, present or future, there is no conjugation of verbs, but usage of grammatical particles (a syllable) like `le` (perfective), `yǐjīng` (already), `hái` (still), `yào` (future), etc.;
* To indicate singular-plural, there are plural markers (a syllable), like men in `wǒmen` (we), or measure words;
* Chinese is an analytic language: to indicate the word's function and meaning in a sentence (see homophones) it relies on syntax - word order and sentence structure; the basic word order is _SVO, subject-verb-object_.
* Chinese has a few articles `zhège / nàgè` (the/a)

### Chinese dialects

_diglossia_: there are many different dialects in China. The behavior is also a bit different between regions and dialects. Chinese talk about `fāngyán`, literally "_regional speech_“. 

These varieties have been classified into seven to ten groups, the largest being _Mandarin_ (e.g. Beijing dialect), _Wu_ (e.g. Shanghainese), _Min_ (e.g. Taiwanese Hokkien), and _Yue_ (e.g. _Cantonese_). Chinese varieties differ most in their phonology, and to a lesser extent in vocabulary and syntax.

<iframe width="640" height="360" src="https://www.youtube.com/embed/0Mk6xX2vZHA" frameborder="0" allowfullscreen></iframe>

The Chinese standard language is the one used by the government and the media.There are two more terms to indicate the Chinese language: `Zhōngwén` and `Hànyǔ` (comes from _Han_, the main ethnic group). It is based on Mandarin (spoke in the Beijing region).

Standard Chinese, also known as Modern Standard Mandarin, Standard Mandarin, or simply Mandarin, is a standard variety of Chinese that is the sole official language of both China and Taiwan, and also one of the four official languages of Singapore.
{: .alert-info}

The need for a standard language is clear from the amount of changes during the years: splittings, dynasties and shifts of power, etc.

## Chinese characters

 There are more than 100,000 different Chinese characters (impossible to count precisely). The number of useful characters, for a literate person however, is “only” between 3,000 and 6,000. The threshold level is 2,000.
 {: .alert-info }

Traditionally, there are 6 categories of characters: xiangxing象形, zhishi 指事, huiyi 會意, xingsheng形聲, zhuanzhu 轉注, jiajie 假借: 

1. Pictograms (600): animals, plants, family: 木 (tree)
2. Ideograms: pictogram + strokes: 本 (origin) root of a tree 木
3. Logical compounds: 安 "peace" (woman 女 under the roof宀) 
4. Shape and sound (largest): 妈妈māma (mother) 女 nǚ +马mǎ 
5. Mutual comment: kao 考 (exam) "old" and lao 老 "aged”
6. (Wrong) borrowing: 而 beard" for "and; but"

The styles also changed among the years and there are different ways to write a single character (just as our cursive and only uppercase variants):

![styles of chinese characters](http://www.ancientscripts.com/images/chinese_stages.gif)

All characteres have the same size (they are in a square).

In the fifties with the rise of Mao, they started to simplify characters by removing strokes. So they have now two systems:

1. the traditional system: going back to the Han dynasty, still in use in Hong Kong, Taiwan and other chinses speaking communities
2. the simplified system: aimed at promoting mass literacy, used in chinese media for example.

About the number of characters:

* 56'000: the number of characters in a dictionary
* 2'000: the threshold
* 3'000: to be able to read a newspaper
* 3'500: what a Bachelor degree student knows
* 6'000: for intellectuals

## Hand gestures 

For numbres, be careful because signs are different. Our way of saying "two" is "eight" in chinese:

![chinese gestures](https://s-media-cache-ak0.pinimg.com/originals/7d/93/1b/7d931bb5b8211c97f8c94dd1a63afb34.jpg)

The hand on the nose is _I am sorry_, the hand with a sort of T is _stop please_ and the last one is _no_ (shaking it): 

![chinese gestures](http://2.bp.blogspot.com/-2BAIrdWvU2Y/VWyRi2S1RSI/AAAAAAAAF3o/xC0sexrTpGs/s1600/smoking.jpg)


## History and culture

###  Ethnic groups

The biggest ethnic group is the `Han`.

> As a large united multi-national state, China is composed of 56 ethnic groups. Among them __Han Chinese account for 91.59% of the overall Chinese population__ and the other 55 make up the remaining 8.41% according to the Fifth National Population Census of 2000. As the combined population of these other minorities is far fewer than that of the Han, they form the 55 minorities of China. ([source](https://www.travelchinaguide.com/intro/nationality/){: target="_blank"})

### Dynasties


<iframe width="640" height="360" src="https://www.youtube.com/embed/ud9QK54vNj4" frameborder="0" allowfullscreen></iframe>

![chinese dynasties](http://chinafashion.weebly.com/uploads/8/2/7/4/8274535/393045.gif?661)

The __Han Dynasty__ ruled China for a solid four centuries, from 206 B.C.E. to 220 C.E. Although the preceding Qin Dynasty unified China, it was the Han Dynasty that kept it together and developed the institutions that characterized most of Chinese history since.

 The __Tang Dynasty__ was one of China’s most cosmopolitan and urbane dynasties, opening China up to a period of foreign influences. The Tang Dynasty was also likely China’s largest and most powerful dynasty in history and is considered the golden age of imperial China.

 The __Ming Dynasty__ is considered to be one of the worst Chinese dynasties, as China suffered a period of intellectual, political, and economic sterility under its rule.

 The __Qing Dynasty__ is often blamed for allowing the Chinese system to collapse and for the country to be humiliated by the West. These things did happen during Qing but it doesn’t diminish from their achievements. It was actually not Chinese in origin, but manchus. They were the first Chinese state to effectively control regions like Tibet, Xinjiang, Manchuria, and Mongolia. They were famous for their masterful diplomacy.

 More about those dynasties [here](http://nationalinterest.org/feature/chinas-3-most-powerful-dynasties-12726){: target="_blank"}


### The concept of face

Lossing face is a huge deal in China. The goal in a relationship is to avoid both parties to loose face.

> Fundamentally guanxi is about building a network of mutually beneficial relationships which can be used for personal and business purposes. In this sense, guanxi is not so much different than the importance of having a strong network when doing business in any country. However, in China, guanxi plays a far more important role than it does in the West. ([source](http://www.businessinsider.com/the-most-misunderstood-business-concept-in-china-2011-2?IR=T){: target="_blank"})

It is pretty much impossible to do business without guanxi. Guanxi relationships can also be far more strong than simple business relationships. But guanxi is not all of business: it will help you, but you also need all the other regular components of business.

In the books we read, offering cigarettes (and not any kind of cigarettes) and drinking alcohol was a large part of guanxi: you need to master it to be successful. 