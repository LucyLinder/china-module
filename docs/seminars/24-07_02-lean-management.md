Geberit est l'un des leaders dans l'application du lean management en Suisse. Cela explique leurs profits faramineux.

# Going Lean - Lean Management 

## A bit of history
The first person that suggested that it is possible to increase profit without increasing sales was Benjamin Franklin (1728): it is sufficient to make them cheaper --> the first consciousness of the basis of lean management

The idea of __interchangeability and standardisation__ is to be able to use something in place of the other. Before, everything was hand-made and a screw for a given chair could not be used for something else.
The one who invented the concept of interchangeable parts and assembly lines while producing rifles was _Eli Whitney_ (1798). The process to make a rifle became cheaper.
In CH, it was _Omega_ which introduced interchangeability.

__Standardized Work and Time__ came with Taylor: the _taylorism_: everyone is interchangeable by distributing the world into small tasks (turning one screw for example).

The first time we heard about waste (mainly waste of time) was with _Henry Ford_ with the model T in an __assembly line__. Assembly line was already known with Whitney, but the new thing was that he wanted his own workers to be able to buy it --> cheap. "I can only sell many of them if I can have prices low enough for my workers to be able to buy it". One of his famous slogans was: 

> You can have the car in any colour you want, as long as it is black (which is the color that dried the fastest).

Here, we are talking about "_gaspillage_" (not trash).

__Process charts__ and __motion studies__ (diagrams) invented by _Frank B. Gilbreth_ (1920) --> documentation of improvement processes. 

__Total Quality Management and Statistical Process Control__ by _William Edwards Deming, Philip Crosby and Joseph Juran_ (1953-1973).

> quality cost nothing ut is simply meeting specified requirements.

"good" is what the consumer is paying for --> means nothing without specification.

__kanban__ is a _pull system_. The idea is to save on everything we do not have. Comes from _Toyoda_ and _Ohmo_. Toyota founded Toyota, which was different since the beginning, because is was made for lean.

See also TPM and Single Minute Exchange of Dies (SMED).

## Introduction to Lean

* _Mura_: I accept that I do something worse than I could do, something not in order. 
* _Muri_: those undesirable conditions makes me work more than I should
* _Muda_: this extra work is waste and should be avoided.
* Hansei: "_on pourrait faire_"
* _Kaizen_: application [continue] des corrections --> continuous improvement
* _Yokoten_: here goes the philosophy. If I could improve this area, there is a good chance I can also improve other areas of the process.

The recognition of the problem is mandatory: if my parents always tell me to clean my room, it won't help as long as I don't understand that with mess I don't have place to play, I loose time finding stuffs, etc.

It is a work of understanding what you can do, not just doing.

------------------------------------------

### exercise on lead-time

lead time: primary processing time 10% and idle/waiting time (90%).

After improvement, idle time could by reduced by 10 percentage points while the primary processing time remains unchanged.

* what is the impact of thses changes for the overall lead time ?
* how much could the throughput time/lead time be reduced (% and absolute) ?

     process      wait
    ¦= 10% =¦====== 90% ======¦

      process     wait
    ¦== 20% ==¦===== 80% =====¦


The processing time has the same absolute value. So, say we had 100 days before, we now have:

    process = 10% -> 10 days
    process = 20% -> 10 days

So now, 20% is 10 days, so the waiting time is 40 days. We get a lead time of 50 days instead of 100 days, so an improvement of 50% !!!

The message here is __look at the waste__/look at the big part/look at the waste.
----------------------------------------

_beware_: with these optimisation, we can produce _faster_, but not more because our capacity didn't change. By working faster, you can have less orders opened at the same time, so it is easier to manage.

### real life example

lead time for a watch. The diagram is the process. W20 is a factory in Fontain-Melon. It is a lead time of 135 days, with a final cost of 1 CHF. In Switzerland, 1 CHF pays about 1 minute of work.
The primary processing time is 1 minute, all the rest is raw material, orders, etc.

In advance is as bad as a late delivery.38 workers equivalent waste in the delayed compnents in thailand during one year.

First, they gained two days from the very beginning. They invited the airline and the customs guy to discuss. Customs: if your components are sealed from your warehouse and that you tell us clearly electronically what is in it, we can skip the inspection. The airline: by paying a bit more, could ensure that at least one flight per day if needed.

Could do the same on Thailand: from the airport to the factory without unpack. Funny thing: people are payed by the number of box. So by increasing the boxes (packaging),  they were happy.

Also, by knowing exactly what is in which box, it was easy for the production line in Bangkok to find the components and to begin directly.

--> all in all, more than 50% faster !

### Dell

Dell is one of the companies that apply lean management very well. The do consumer-oriented service: machines on-demand, just-in-time.


Video: 40 seconds for one cycle, about 10 to 13 machines. Eliminated: motion, stocks, ... Here, the bottleneck is the girl, because she works at 100%, she has no time. To augment the lead-time, you need a new girl. 

-> look for the bottleneck, the limiting factor

## Principles

__Muda principle__: core objective is the elimination of waste whatever
__PULL principle__: the value-add processing (production)  should begin only after the customer order
__Minimum lead times__: as a result of the MUDA

Time is the master criteria of waste. _Lead time_ is the easiest and most effective way to identify and eliminate waste. 

_Information lead time_: time to react and to take decisions
_Material lead time_: time until changes take effect.

information+material lead time is the time until changes in demand are dealt with. 

Other important principles:

* __Kaizen__: continuous improvement
* __Poka Yoke__ foolproof processes
* __Gemba__: walk and see the problem on site (not form the office). It is a philosophy, the boss go see the problem.
* __5S__: good order and cleanliness at every workplace.

So: reduce lead times and apply PULL concepts to be on the right way.


### The 5S

* __Seiri__ (sort out) remove all unecessary tools or stuff that we do not use
* __Seiton__ (stabilize) for the things that we need, we have a dedicated place
* __Seiso__ (shine) take care, keep all things clean. Taking care of the body is also part of it. It is part of the dedication, the commitment -> "I take care philosophy".
* __Seiketsu__ (standardize) do the first step for all station
* __Shitsuke__ (sustain) continuous improvement


### 7 Kinds of waste

__D__efect: quality problem<br>
__O__verproduction: producing 5 pieces but the client pays for 4<br>
__W__aiting<br>
__N__on-value adding processing (overprocessing): the quality is higher than what the consumer needs, we overengineer the product the consumer will pay for (nice polishing, added features or a better uptime than the SLA stipulates)<br>
__T__ransport: if you transport, then you have something at the wrong place (except transport to the consumer, because it is paid by the consumer)<br>
__I__nventory: means that we have the material at the wrong time<br>
__M__otion: means a lack of ergonomics<br>
__E__RP related waste: having the wrong data is the most relevant waste which you even don't see ! Wrong data, wrong planning. Can be the cause of overproduction and other wastes.


## Characteristics

* high flexibility in manufacturing
* high quality in manufacturing
* low costs per unit
* low inventor
* quick response to market changes
* high motivation and joint responsibility of employees
* flat hierarchies: bosses are waste, they don't bring value to the consumer

Easier to apply the lean principles in the east, mostly because of the cultural differences:

|West|East|
|----|----|
|individuality|collectivity|
|freedom|discipline|
|division of work|task identity|
|seriousness|playfulness|
|everybody on his own|together|
|more technically educated people|more people trained on the job|

In Switzerland, everybody is a little Guillaume Tell. If the boss proposes something, we instinctively say no, then listen and maybe get convinced.

Task identy means also that there is a holistic approach: take the body for example. If taken care of, will help the company, thus the yoga on the work environment. Playfulness is also important, if I don't have fun at work, no point.

### The smart factory - Hambach

A cross or plus-shaped batiment. Not very efficient from an achictectural point of vue. But the cross makes supply easy: they are in a square building with little bridges to the branches. No warehouse, but bring new material and take away the empty palets.

For small pieces, "Milkman" organisation. People walking around and fill missing things (kanban system, so easy to detect what is missing). 

The world-record for the _lead-time_ in assembling a car (3h) ! If you include the body (carrosserie), about 7h. 


### SMED

Reduce setup time at a minimum (waste, since the machine does not produce).

> SMED (Single-Minute Exchange of Dies) is a system for dramatically reducing the time it takes to complete equipment changeovers. The essence of the SMED system is to convert as many changeover steps as possible to “external” (performed while the equipment is running), and to simplify and streamline the remaining steps (source: wikipedia)

For example, in Geberit all molds and materials for injection are standardized, so you replace/switch between them very easily. The next things are already available next to the machine.


### Andon boards

Shows how many goods are to be produced today, the trends, were you are in number and schedules. 

==> real-time visibility on the problems !

Minimally, Andon is a system to notify management, maintenance, and other workers of a quality or process problem. The centerpiece is a device incorporating signal lights to indicate which workstation has the problem. 

We saw that in Huawei for example (little lights: red, orange, green) next to the machines.


## Conclusion

MUDA is not about going faster, but going smarter !


# Lean Manufactoring

Lean management applied specifically to manufacturing.

PPM / DPMO = defect per million opportunities. 3.4 PPM is 99.9997% non-defect rate.

PULL: the manufacture receives the order and _pull_ the supplies from the suppliers. 

If one station (or supplier) fails, all stations stop. In the PUSH model, if one station stops, then some stations will have _backlogs_ (should produce but can't) and other will store pieces they accumulate.

## Kanban 

means "information tag" in japanese. The idea is to schedule production based on visual signals: empty boxes, empty spaces, kanban cards...

little or no stock. Some machine will run only on demand so be idle often.

## Lean logistics

### 7-R

Right ...<br> 
... material<br>
... time<br>
... place<br>
... quantity<br>
... quality<br>
... customer<br>
... cost

operational logistics ("box moving") and dispositive logistics (planning part)

### The four P of marketing

* Product: design-to-cost
* Price: price given by the outside, as engineers we have nothing to do
* Place: sales organisation, not our job
* Promotion: marketing department

Price given by the outside, so we do a _design-to-cost_ approach: our job to develop a product that match those constraints.

> Innovation is creativity to earn money.

70% of the price is determined by during the product development, i.e. before the first piece is sold. 

# Conclusion: the lead-time gap

![lead-time gap](http://slideplayer.com/4594103/15/images/9/Lead+Time+Gap+Problem+of+most+organisations+is+time+taken+to+procure%2C+make+and+deliver+is+longer+than+the+customer+will+wait..jpg)

* for processes "inside" the customer's order cycle, __make-to-order__, PULL
* for processes "inside" te lead-time gap, no choice but ot __make-to-stock__ (PUSH)

How to reduce the lead-time gap ?

* ask the customer to wait longer... Nope. Not if you have concurrent.
* reduce lead time, for example by reducing waste.


## Conclusion

* Make-to-order (PULL)
* Muda => reduce waste
* Reduce lead time 

Lead time is important because it influences the other two and is easily measurable (time vs waste) => Lead time the main driver to make an organisation lean