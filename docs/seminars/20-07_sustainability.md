__Innovation and sustainable development__

By Claudio R. Boër, vice-president of SUPSI and part of swissnex.

> Every act of creation is forst of all an act of destruction
> _Pablo Picasso_


We must be able to measure something, otherwise it is just nice words.

CX: aerodynamics of a car. But we never talk about it.

## Innovation

Creativity (innovation comes from new ideas), everybody gets ideas every day. But innovation is the process that transforms ideas into something with value, more difficult to achieve. 
Creativity <-> innovation is a continuous process. A company needs to reinvent itself continuously to survive.

_Tassey_: extended product lifecycle, the red is the expenses, the blue line is the money in.

everytime there is a transition to electronics (for example for cameras), the Japanese come it.

_About generations_: for the Iphone for example, even though we are talking about iphone 1, ... 7, we are still at the first generation.
cars: electric cars is a new generation. Motor changes, the tank becomes a battery, and so on.

Innovation is not only about technical knowledge, but also about general knowledge, open-mindedness, etc.



What was the real innovation of __Apple__ ? iTunes and the iPod, the idea of selling only one song and have one product coupled with a software library. Playlists were made possible. Note that japanese also had iPod-style technology before, but didn't think of the iTunes store... (in my opinion, the Apple II and personal computer was the real innovation ^^).


## _A little story about innovation_: the watch industry.

In mid-70s, CH was the world leader. All watches had the same messages: we are the most precise timekeepers, nobody else can match our craftwmen. An interesting message, but has flaws: same message for all brands, so there is no differenciation except the design; you can destroy the message by creating a cheaper and more precise watch.

Exactly what happened with the event of electronic quartz watches: cheaper and way more precise. Swiss watchmakers refused this new invention, but Japanese didn't hesitate to accept this new technology. The result: the Switch watch industry lost between 35% and 50% of the world market share. 

Being the best is not enough, you need to keep it that way with constant innovation.

Cheap but fashin watches, such as swatch, represent about 90% of the total number sold and 50% of the total value. Make the math for traditional and expensive watches. The japanese came in the first market.

After the japanese conquest in 1980, the Swiss share was at 0% for the first layer (< 100 CHF). On the second layer (< 700 CHF), 0.5%. Finally, 95% for the very expensice watches. We are not even dominating the top layer !

Nikolas Hayer (swatch) believed strongly in marketing. He wanter to reconquer the lowest layer of the market. Reasons ?

* mass production possible
* young people buy cheap but good products
* if you conquer the lowest market, the competitors will need to invest to move to the other ones, so its a win-loose.

So, Hayer launched the _swatch_: cheap, very high quality, 100% Swiss, never faked. A normal mecanical watch is made of at least 150 components. What swatch did is a quartz built up layer from layer to allow automation.

Result: today we have 15% of the lower layer, 25% of the middle layer and still 95% of the upper layer. Also, the cherry (> 100'000 CHF) number of pieces sold increased to thousand pieces. What changed ? Chinese buyers came into play !

__System 51__: a mechanical revolution. A mechanical/automatic watch with 90 hours of autonomy (very good !).

They lowered the number of components to 51 and it was the first mecanical watch made entirely automatically and in Switzerland, a huge step for the swiss industry: it is possible to create good mechanical watches at a very low price from Switzerland.

__Belenos__: co-founded by Hayer. One of the projects was a battery for electric cars of 50 kg (in a tessla car, at least 100 kg) which allows a range of 500 km (tessla ~100km) and with a production cost < 1'000 CHF.

Geely (btw. now has Volvo, so is also in the high market), the chinese car manufacturer, worked with Belenos to make this battery possible. in mid-May, Geely and Belenos signed a declaration of intent for use of the new battery technology. So it will become real, they are currently creating the first assembly lines for testing.

__Global innovation index 2017__: who is leading innovation ? Switzerland > Sweden > Netherlands > USA > UK. What is interesting: in the upper-middle-income tier, China is the first.

![gloval innovation index 2017](https://www.romania-insider.com/wp-content/uploads/2017/06/Leaders-in-Innovation.jpg)


## Mass Customization

distionction: _waste_ can be reused/recycled, but _trash_ is just useless garbage.

1/3 of the energy consumption is consumed by the industry.

Mass production appeared with the Ford Model T in 1913. Note: they used bio-fuel by then, which was easier to make. At
the time, only a black model, no customization possible. But why black and not red ? Because black was the colour that dryed faster.

Shoes: about 80% of the produced shoes will be sold in the end. 20% unsold ! 4 bilions pair of shows need 5 power plans, each with an average of 500 MW. If you add up, you will need 25 power plants to produce all the shoes in the world. Out of that, 5 power plants will produce unsold products.
(in the US, 572 operational coal plants with an average capacity of 547 megawatts. In the world, how many (any kind: coal, electric, nuclear) ? > 65'000 power plants).

That's the paradox. Solutions ? (answers from the students)

* by more shoes :)
* give shoes to poor people (problem: brands don't want to give unsold shoes + some shoes wouldn't be useful for African people for example). Some brands repackage the shoes, strip all the brands and resell them.

The material for unsold shoes can be reused, but how about the waste of energy ? That is the real problem !!

The problem is mass production, which tends to produce more products than are really needed. It is _market oriented in an approximate way_. The "approximately" is creating the waste: if we could predict more exactly the trends, volume and time of the market, we wouldn't need to produce more to cover the risks.

In mass production: design, make, sell. In mass customization: design (this all options) -> sell -> make. Here, we return attention to the client instead of the focus on the market. The business model is thus changing.

We enter in what we call Mass Customization
{: .alert-info }

_Push_ model: mass production; _Pull_ model: mass customization, the client triggers the production.

So, the idea is to produce __only what is necessary__, __when__ it is necessary, on the consumer demand.

Still a lot of questions and problems: shipping for example (one shoe vs 10'000 shoes at once), fluctuations in the demand so unused space and workers, not pushing the product could mean less sells, etc. Also, how to ensure the whole process is triggered at the moment of the order ? It seems that the pieces should by in stock, then only the assembly would be done on demand (so in the end, it is quite the same).

See the 
EUROShoE project, the adidas Futurecraft 3D printed shoes

New shops: one sample of the shoe for each model, so you can touch. Then, a 3D scanner will be used to determine which size, but also which width and shape would fit best. You could also use augmented reality to have an idea on how it look on you.


To be sustainable, you also need to design not only for what the consumer wants.

## The new industrial revolution

(Peter Marsh), the 5th revolution