The PDFs are available [here](../resources/pdfs/companies/13-07_Geberit.pdf){: target="_blank"} and [here](../resources/pdfs/companies/13-07_Geberit-GPS.pdf){: target="_blank"} 
{: .alert-info }


* Sales mostly in Germany (~30%) and Nordic Countries (10.9%), then Switzerland (~10%) => most sales in Europe.
* Key dates:
    * 1874: opening of the plumbing company
    * 1997: Geberit family sells the company
    * 1999: successful IOP on the Swiss Stock Exchange
    * 2015: acquisition of Sanitec, Finland, which is exactly the same size than Geberit
* The acquisition of new enterprises and such was a huge job for the IT department, mostly because the SAP needed to be merged (or at least to communicate with each other).
* are present in nearly 50 countries
* goal: improve the quality of life through innotative sanitation
* sustainability in all areas of the company. Social and environmental impact has been reduced, even though they had a step back with the beginning of the ceramics business. Also social projects involving Geberit apprentices since 2008. 
* very strong expansion selection
* B2B _"push/pull" concept_: don't sell to end users. Push to wholesalers; sort of technical pull by supporting and training architects, installers and planners. 
* 3 years ago, Geberit only, but now more than 10 other strong brands.
* three pillars: piping systems (29%), sanitary systems (45%), sanitary ceramics (26%)
* net sales 2016: CHF 2.8 billion
* they do as much as possible by themselves: draining and flushing systems, pipes, etc.
* specialized in silent products: concert houses for example not very nice if you hear the rain because of a noisy drainage system. Not so many companies in this line of quality/silent work
* long-living products: between 20 and 50 years.
* usually, China plants produce for the China market. Only some goods are rapatriated to Europe.

## GPS 2.0

[insert picture here]

### PULS

__PULS__ stands for Production and Passion: decisions are made at the lower possible level (Shop Floor Management, decisions made where the action is). Daily onsite management ensures commitment, discipline, fast reaction and takes management to the front lines. The responsibles are the _shift leaders_, which are only at the 4th or 5th level of the hierarchy. 

KPIs are standardized and every plant is compared with the same indicators and level of requirements. 98% service quality is the lowest threshold.

The advantage ? 
* An immense reduction of coordination time, 
* fast reactions and results, 
* significant higher productivity (increase by 46%), mostly because people feel important and more motivated

A plant was baught from Geberit some time ago. After applying the Geberit methods: one quarter less people, increase of productivity by 50% and a large decrease of backlogs.

### CIP 

The most important thing is __CIP__ - Continuous ImProvement. Day by day, small improvements to get better, even in small details. 

Feedback meetings, the goal is to do little single steps but with a fast implementation and with immediate corrections and results. What is frustrating people is the impression that bad conditions are stable, that nothing change. This method also gives the people in the shop floor level the impression that anything is possible !

Observation is at the center of this method: observing, finding a root cause, changing a small thing, then observing again.

Not the same for employees, in the sense that they tried to make the jobs evolve continuously, but people are not good everywhere. It is better to keep people where they are good at.

### No-Touch in administration

Avoid worthless paperwork and actions, concentrate on strategic issues. 

In SAP, there are many steps from pruchase to invoice processing. The idea is to have to do something "by hand" as less as possible. Now, 72% of all processes are now automated (in SAP).

For suppliers, it means EDI-readiness. 

## Geberit supply chain concepts

In 2010, decided to build a big logistics center in Pfullendorf. All other plants are bringing the finished goods there (they have litteraly no stock here for example).

This means that if I order something in the same city than the plant, the goods will still go first to Pfullendorf first then come back. But it is easier, since plants don't have to manage transports anymore. And the central planing of logistics allows for many optimisations, so all in all, there are less kilometers travelled in the end.

About 80% of the work in the logistics department is automated as well.

"Changing processes does not mean killing jobs. What kills jobs is loosing money": I don't really agree with that, but the teacher seemed very sure of himself. Just after this statement, Geberit still confessed that in the past year, they went from 250 to 230... 

__Standardisation is the key__: for example for packaging, for raw materials, etc.

> a highly automated stable process is the only way to produce (and keep being competitive) in Switzerland

About the last point: see the _Swatch system 51_, a watch composed of 50 modules and only one screw. The whole watch is assembled automatically, no human intervention at all ! The swatch is an automatic and even the configuration is done with lasers instead of by hand. This is great, because it mans we are able to keep the watch creation in Switzerland. 


# The visit

* automated transports: little cars are carrying palets and products all over the place. Right now, they follow big yellow magnetic lines on the floor, but they intend to switch to GPS-driven robots that are capable of going freely between any two points.
* The R&D department is centralised in Jona
* they are very proud to have done one insourcing from Germany in the last four years
* during the visit, we saw some garbage on the floor: "this is typical of an unstable process; the team leader should notice it, observe and then write the problem on the fixes&ameliorations board".

> Measuring afterwards is useless, you just need to ensure your process is stable to guarantee the quality of the product

In Geberit, the processes are constantly changing. 
They are really neatly organised: 5S, kanban cards on each machine (and not on a dedidated space as usual), etc. Changing a process can be very minimal. For example, they had trouble with the palets: too many stocks, badly organised, etc. So they decided to use a _leaner_ approach. They now have two trucks full of palets. One is near the entrance. Anybody can take palets from it and when the first one is empty, the second one takes its place and the empty ones is refurnished automatically be the supplier. They don't keep an exact count of the palets, just receive a bill at the end of the month. This change simplified a lot the process. 
They did exactly the same for materials near the machines: only two containers for each material + rotation.

The "just do it" and "observe, plan, execute, ..." are a simple system: one board, people can write problems they witness. If they also have a solution, they write it down and decide if it is easily feasible. If it is, it goes to the "just do it" part of the board. If not, it goes to the other part and at least once a week someone will take care of it.

What is interesting is that this system was very efficient in the beginning, but after some years it just became a routine. People are not motivated by it anymore, so the efficacity dropped. They think of stopping for some years then introducing the system again, to keep changing.

