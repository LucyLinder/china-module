by Choon Huat Tan, Head of Finance Asia Pacific.

## Presentation

* 22 years in China
* ~50 countries with sales organisation
* 30 production sites: 26 in EU, 2 in China, 1 in India and 3 in the US. Sales companies more common outside EU
* Far East/Pacific is the 12th of 13: 2.8% of the sales, so very small
* focus on sanitary products: not a conglomerate, but choose to stay in a small business area and do it well -> strong in a niche (more than 50% market share)
* 2-2.5% of R&D investments each year
* 20 people for R&D here, not developed a full new product, but adapt corporate products to the market

When they started in China, they needed a joint venture to enter the market. First SFD in 1999: in Asia, plumbing are a bit different (you flush, it goes directly to the neighbors through a wall below the toilet) -> began to PUSH the EU technology/system to Asia (CFD => SFD - Same Floor Drainage).
In China, those concealed toilets are less than 1% of the market. A challenge !

Also for roof drainage, _syphonic roof drainage_ instead of gravity.


Squatting toilets are still here, but not in new buildings. Many think it is more hygienic...

Geberit is not offering just a single product (location of the toilet depends on the pipe whole), they offer a complete system. A wall hides the plumbinb and the toilets can be put anywhere (or at least elsewhere).

Factors of success ? Reliability. Geberit systems work and last, the quality is reknown. R&D investments and innovations (invented many stuffs that we use everyday) is also important.

Factory experts come when there is a need for change in production. Everything is controlled by the HQ and there are norms that any plant need to apply strictly.

Main components come from EU, just some raw materials come from local suppliers. Each plant produces some product lines, so there are a lot of import/export.

Technology has become one of their differentiated factor. So they won't just create "basic" toilets for the Asian market (which would certainly work, quick fix to get market shares). Instead, they are really trying to impose their technology.


