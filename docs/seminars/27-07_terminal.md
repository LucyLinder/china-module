__SECT__

2002 - SECT, __S__hanghai __E__ast __C__ontainer __T__erminal is created.

* A joint venture: SIPG & APM terminals / public and private
* first to be able to handle 1 mio TEU (20-feet equivalent unit) in the first year.
* 155 hectares of land: land site (98 ha) and peer site. The empty blocks are close to the in/out gates. Import, export and ??? area. 
* TOPS 5.0 system -> developed by themselves
* lean operation and agile management
* E-RTG conversion program


* largest port in China
* #1 worldwide in container throughput for 6 years straight 
* 3.80 mio TEU in 2016 (best performance ever), 3.85 TEU in 2017?
* whole shipping industry not good enough: one of the biggest client company went bankrupted; many merges and rachats... The market is over-capacited
* the made in China is not made in Taiwan or made in Cambodgia...
* 2 shifts a day, 24/7 -> 12 hours shifts, mostly because of the tide which changes twice a day. They are more than needed at some places to allow them to rotate during the shift and take some rest.
* Outsourcing: more than 700 persons. Canteen, tracing, etc. 
* 
