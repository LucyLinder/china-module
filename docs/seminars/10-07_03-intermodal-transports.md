
the PDF is available [here](../resources/pdfs/10-07_03-intermodal-transport-system.pdf){: target="_blank"}.

## Miscellaneous

Optimize costs: transport, handling, etc. For example, the chinese _square fruits_ (such as watermelon) that are easily stackable/transportable.

Logistics has a very high impact on final costs. Transportation is about 5% of the sale.

![days required to circunvant the globe](https://people.hofstra.edu/geotrans/eng/ch1en/conc1en/img/circumnavigation.png){: style="max-width: 100%"}

![](../resources/graph-transport-modes-comparison.png)

In terms of external costs (pollution, etc.), the most convenient mode is the electric rail.

In Europe (2014), transports accounted for about 33% of the global emissions of CO2. 

Containers have an normalized ISO _Container Identification System_. Parts of the information are the length, height, type, owner.

A container spends more than 50% of its life idle and empty.

Trailers and "lines" have different shapes, and railways in Europe limit those shapes depending on the regions.

Movements for loading/unloading trucks on rail: vertical movements seems more "logical", but it means you cannot have electrical lines about the rails ! So horizontal movements are often used instead.

The Gothard is the World’s Longest Tunnel used for transportation: 57.1 km long.

## Definitions

* __Intermodal transport__: freight transport using two or more transport means, but using the __same freight transport unit__, without any handling of the freight itself when changing modes.
* __Unimodal transport__: freight transfer using only one transport mode; 
* __Multimodal transport__: freight transport using more then one transport mode.

Difference between containers, swap bodies and semitrailers:

![container, swap body and semi-trailer](../resources/container-swapbody-semitrailer.png)

## EcoTransIT World 

Means Ecological Transport Information Tool worldwide (ETW).

EcoTransIT is an online emission calculator and enables users the quantification of energy consumption and emissions of worldwide freight transport by trucks, trains, ships and airplanes ([source](http://www.infras.ch/en/projects/ecological-transport-information-tool-for-worldwide-transports-ecotransit-world/)).

The tool shows only the environmental costs, it is up to the enterprise to compute and then compare the costs of each mode.

## IFDSS

Intermodal Framework Decision Support System. Software to help building and assessing intermodal solution scenarios.

You define your transport scenario and then get key performance indicators (KPIs) and other assesments (economic, environmental, sustainability, etc.) to help making good decisions.
