PDFs about batteries: 

 * [Introduction to the EV batteries (project)](../resouces/pdfs/21-07_01-introduction-batteries-EV.pdf){: target="_blank"}
 * [Understanding Li-ion batteries and BMS](../resouces/pdfs/21-07_02-understanding-Li-ion-battery-and-BMS.pdf){: target="_blank"}

The PDF about the economical/environmental impact of EVs in China is available [here](../resouces/pdfs/21-07_03-economical-efficiency-EVs.pdf){: target="_blank"}
{: .alert-info }

  
_Effects of internal battery resistance_: a
 battery with low internal resistance delivers high current on demand. High resistance causes the battery to heat up and the voltage to drop. The equipment cuts off, leaving energy behind.

# electrical cars in China

China is leading the transition to EV, Electric Vehicles:

<div class="atlas-chart" data-id="rykSiNByZ" data-width="640" data-height="449"><img src="https://www.theatlas.com/i/atlas_rykSiNByZ.png" style="max-width: 100%;"></div><script src="https://www.theatlas.com/javascripts/atlas.js"></script>

China is now aggressively championing the EV. The world’s second-largest economy wants 11% of all car sales to be electric by 2020. This should add up to nearly 3 million such vehicles sold annually. Sales of _“New Energy Vehicles,”__ as they are called in China, accounted for about 50% of all plug-in electric vehicles sold globally last year (and many were manufactured by China’s own automakers).

To accelerate the transition, the government is investing a lot of money. For example, it will deploy 100'000 public charging stations in 2017 alone, almost doubling the current total of 150'000. 

In November 2015 the Chinese Ministry of Transport, the Ministry of Finance and the Ministry of Industry and Information Technology jointly released a new regulation, which obligates local governments and relevant stakeholders to promote the integration of electric buses in public transport fleets. According to the regulation, new energy buses deployed have to comply with energy efficiency and new energy vehicle standards. In Beijing, for instance, a share of 80% in 2019 is anticipated. 

__The GREET model__: Sponsored by the U.S. Department of Energy's Office of Energy Efficiency and Renewable Energy (EERE), Argonne has developed a full life-cycle model called GREET (Greenhouse gases, Regulated Emissions, and Energy use in Transportation). It allows researchers and analysts to evaluate various vehicle and fuel combinations on a full fuel-cycle/vehicle-cycle basis.

    well to wheel (WTW) = well to pump (WTP) + pump to wheel (PTW)

For EVs, most of the work is the transport from power plant to the consumer (64%, red area). 
HEV (hybrids) are the one with the less consumption considering WTW -> the most energy saving technology ! EV are just after the HEV. But in terms of WTP, EV are the worst: the energy needs to travel a lot to the cities. In China, the coal is only in the north, and many huge cities are from the south. 

In China, __more than 80% of the power comes from thermal plant__ (2011). In the US, it is about 60%. In a thermal power station fuel such as coal, oil or gas is burned in a furnace to produce heat (chemical to heat energy). Nuclear poewr is only 1.82%.
In Switzerland, we have 60% of hydro-power and more than 30% nuclear power.

The efficiency of power plants is equivalent to those of USA and the Greenhouse gas emission is also equivalent.

Since electricity in China comes from coal and other fossil sources, the WTW is very polluant; even for EVs. 

> In Coal-Powered China, Electric Car Surge Fuels Fear of Worsening Smog
EVs charged in China produce two to five times as much smog-forming particulate matter and chemicals as gas-engine cars, studies find ([source](https://www.scientificamerican.com/article/in-coal-powered-china-electric-car-surge-fuels-fear-of-worsening-smog/){: target="_blank"})


So, EVs are efficient, but produce a lot of pollution as well, since eletricity in China comes from coal (thermal plants) from the North. One advantage though is that pollution is not located in cities. So __EV is not that environment-friendly in China__ !
But if we also consider VOC (Volatile Organic Compound), EV become more interesting. So it is difficult to say if EV is environment-friendly or not.

> the environment-friendliness of electrical parts depends on the profile of energy production in each countries. 

Now, the cost of EV batteries is very high (but it will drop in the long-run). The price by km is also higher for EVs and hybrids (if we take into account the price of the vehicule and the price for pollution).

The nationwide pollution cost is higher for EVs than for gasoline vehicles. But it drops in city areas, since there is nearly no emission during use (only for electric production).

_So, if EV is higher (cost and pollution), why the government want to invest in it ?_  

1. The government wants to cut the proportion of coal consumption. Wind and mainly nuclear power plants instead (uranium bought in France). So the cost of EVs will drop in conjunction.
2. The price for batteries will drop, so the economical efficiency for EVs will be more and more interesting.


> Mainland China has 36 nuclear power reactors in operation, 21 under construction, and more about to start construction
> In 2017, it will complete construction of five nuclear power reactors and start construction of eight more, according to plans released by the country’s National Energy Administration (NEA). 

According to this study, where we consider (a) monetization of emission, (b) maintenance, (c) depreciation and (d) fuel, only EVs would be the best solution for Switzerland. So why so few EVs in our country ?

1. China is supporting the transition to EVs. For example, in big cities, EVs are "sponsored" and so cost about the same as regular cars. Not as much in CH. Also, in big Chinese cities, you need a permit before buying a car/getting a plate. The chance to get a licence is higher for EVs (~50%) than for fuel cars (~1%).
2. Missing charging stations, which is a very repulsive at the moment. Also, the time for chargin is 1 hour...