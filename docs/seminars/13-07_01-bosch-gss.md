The PDFs are available [here](../resources/pdfs/companies/13-07_bosch-global-sourcing.pdf){: target="_blank"} and [here](../resources/pdfs/companies/13-07_bosch-SCM.pdf){: target="_blank"} 
{: .alert-info }


The Bosch packaging factory, GSS, is located in Beringen. See [boschpackaging.com](https://boschpackaging.com){: target="_blank"}

## Introduction and key figures

* 95-99% of the sales are outside of Switzerland. You will find many products installed here and installed by them in China.
* Bosch is a huge company: about 70 billions worth
* 99% of the smartphones avec pieces from Bosch
* 4 divisions: mobility solutions, industrial technology, energy and building technology, consumer goods.
* packaging is divided into: pharma, food, ATMO. Design and manufacturing of packaging machines. 
* What they do:
    - _process technology_ for liquid and solid food. The Mars bar for example. They provide the machines for the packaging part, not the whole pipeline. But packaging is the most important/difficult part and takes the most space in a factory. One system is about 70'000 biscuits per minute (for one line).  
    - primary packaging_ for liquid and solid food
    - _secondary packaging_: cardboards and such, to ship the food to the market.
* part of SIG until 2004, were they have been bought by Bosch.
* able to attain 2'000 wraps per minute with a single line. A machine costs about 600'000 CHF.

Packaging is the first thing you will throw away ^^. But it also protects and conserves the food, gives product information, allows for easier storage and transport, is a primary vessel for the companies image. So they are not only waste after all.

## GSS

From _Christoph Wilhelm_, Global Sourcing Services AG.

> Global sourcing... in challenging time

__GSS:__

* global manufacturing and sourcing partner
* other companies are designing the products, they are making the products.
* combining the global supply chain from all over the world: for example, some plastic pieces are not available in China in good quality, so they are imported from Eastern Europe and assembled in China.
* strong focus on high end applications. 
* two sites: one in China, one in Switzerland. 
* GSS is not working only for Bosch, but also for other big companies like swarovski, varian, trumpf, etc. 

__Quizz__:

* _from which country comes the company with the most patent applications ?_ 1) Huawei, China; 2) Qualcomm, USA
* _which country is the bigest global oil producer ?_ USA (then Saoudi Arabia).
* _what do the group of indian people celebrate ?_ The space program of India. The chief of the program is a woman. India ecomes the 4th country with a successful Mars-Orbital-Expedition. Their mars mission cost less than the 'Gravity' movie by the way. 

=> The business world of tomorrow will look differently than today !

The changes and the adaption to these changes will become crucial - for individuals as well as for companies. This will be true especially for Global Sourcing.

## Changes in the world

__The fertility rate is declining__, even in India and China. The population is aging. So what is the impact of this ? For the economy to grow, you need either additional workforce or to increase the productivity... The real problem of China right now: it is getting older more quickly than getting rich ! This is also why China's government is focusing on robotic in the actual 5-years-plan.

There is an area where there is still growth: Africa. For labor-intensive work, this means that the most interesting market is Africa, not China or India.

__Urbanisation__: about 50% of the world's population is living is cities. Close to 50% of the growth ahead will be from the big cities from the emergent market (Asia is 42% of the top 200 cities). Actually, we also see that the center of gravity of the worldwide consumption is moving more and more to the east. 

__RCEP__: Regional Comprehensive Economic Partnership. Around 40% of the global trade, and Switzerland is not part of it. 

By 2025, the emerging economies will grow 75% faster than our economies. Nigeria a real booming area, thanks to oil. The east part of Africa is coming as well, especially in the drones and food sector. The South America (Chile, Peru) are also emergent. 

## China

_Strength: _
* the world's workshop 
* soon te biggest national economy 
* excellent infrastructure 
* a middle classs of >300 mio.

_weaknesses:_
* access to the www
* justice system / IP
* salary & cost development
* corruption

_Chances:_
* increasing spending power and more private consumer
* investments in future technology
* free conversion of the CNY

_threats:_ 
* unknown (real) debts (of the banks) 
* impact of the "+one child policy"
* claim for leadership of the CP
* more (aggressive) foreign policy

Labor costs means nothing by itself. You need to look at the labor productivity as well. China's labor is more expensive than in Vietnam, but their productivity is way higher. 

> India isn't a manufacturing power country like China. Terry Gou, Chairman of Foxconn, January 2017

China is nowadays the biggest automotive market. It is now dominating the global automotive market ! And they now try to quick out Germany and the US to become more independant in terms of cars.

China is aggressively exploiting the world.


> When in China, count the number of Porsche you will see on the street. Now the question: for which market will you design the next product ? The aging population, or the booming markets ?



_Note:_ fear that the machines and automation will take away our jobs. But the same fear was present during the invention of the steam machine. And in fact, we are short of engineers right now. So the type of jobs will shift, but we clearly won't be out of jobs.

# Sourcing in low cost locations

Talk from someone from the purchasing department.

You need the network, going there is not enough.

## Responsibilities of procurement

__internal expectation of procurement__:

In the past the reputation of the purchasing department wasn't high, but nowadays the expectations and requirements have changed, the sourcing market has globalized, we are exporting most of our prodcucts for the pressure is high.

1. cost: the lower possible material and process (get the goods quickly and easily) cost
2. quality: consistently exellent quality
3. delivery: short lead times and on-time-delivery

Most of the time, you can bet two of those, but not the three at once. In China, you will find cost and maybe quality, but not delivery for example. 

Enginner-to-order: the need for supplies is huge from the point of view of the purchasing department. 

Sourcing in low cost locations - outsourcing of added value.

Bosch packaging wants to focus on the core competencies: delivery complete systems (not single machines). This means Bosch needs to allocate their resources accordingly --> You cannot do everything internally, most parts need to be bought from the outside. They buy about 80% of the parts. Good, mature modules are more difficult to find, so some of them are still done internally.

Outsource modules (rather standard modules, which is the added value) to suppliers which do it more time and cost efficiently.

## Comparisons of sourcing regions

Decision criteria for sourcing regions: costs, lead times, supply chain complexity (you want a predictable demand, a low inventory level, the module maturity). 

low inventory is less expensive, but if you have an unpredictable demand, your inventory should be high to buffer the variations.

Those critera are used to decide which module will be sourced/produced to which region.

__Implications of supply chain on daily business:__ 

_demand-driven material planning _instead of consumption-driven, but purchasing has 4 weeks time (according to project planning).

Two cases: 
* stock keeping unit: safety stock to compensate the 4 weeks
* project delivery: forecast to supplier what we expect him to deliver


Supply chain: a) sales, b) engineering, because each customer order is sort of unique, so you need to configure/design the whole system, c) purchasing the parts, d) assembly. 

After engineering part, engineers will enter the parts needed for the products into the SAP. The purchasing department quicks in (BOM: _Bill of Material_).

6 to 7 month for an order. The engineering part can take two month. If the parts take four weeks to come, this is buffered by a stock here in Switerland/Bosch.

Finding the piece you need is not the problem, but you will need to choose between cost and delivery time.

Parts are easy to source/to find suppliers for. The modules are more problematic, because it requires skills and competences. So you cannot source a module to another company whenever you want; it is better to stick with the ones you are currently working with. 

They just introduced a sort of Kanban system.

A full container from Shanghai to here door-to-door is 2'800 CHF. "my advice to you: throw away the logistics costs; that is never the thing that is blowing up the project"


__sourcing: why China?__

* the cost ! But only if the demand is predictable and the module is mature (it will stay the same, won't need to be redesigned in the future)
* cooperation with GSS: GSS is not a 'pure' chinese supplier: communication and coordination through Mr. Wilhelm,  not intercultural issues
* quality is not a problem dut to 100% quality-check
* small assembly hall and warehouse in CH: fast response to quality issuse and /or fast delivery
* supplier-dependency: boshc has no own supplier network in China. Just the GSS partnership.


# Visit

Bosch mostly builds systems from component that are engineered but not manufactured by them. So the added value is mostly in the system assembly and the quality of the design.

The main buyers are in Europe and North America, the suppliers are in East Europe and Chine.

The belts, glue station, etc. are from China, so they are not making just the small parts.

They mainly use stainless steel because the machines should be washable. Indeed, most clients run the system all week long and stop them only about once a week, mostly for cleaning.

They need about one year test for a new chinese ssupplier. During this time, two lines are working: the chinese one for testing, a Swiss one for production.

They guarantee 98% line efficiency and > 99% for individual modules. New modeles developed every 5 to 10 years.

One module is very flexible, si it can be used for any product (size, height, etc. is adaptable).

We saw a beautiful old school Bosch machine from the 1960s: all mechanical, no electronics. But it is able to do things that a new high tech machine cannot (see the _gullen digestive classic_ biscuits, Spain, a cylinder packaging folded at the top and bottom). One need so many coordinated movements to do the folding that the electronic controls needed would be too much.

One assembly takes seven days: one new machine every wednesday. They work by team with a rotation planning for each day. 

Industry 4.0: connected machines, touch screen to reprogram and monitor the system.

## About human resources

* 70 engineers in the technical office in Berlingen, 100 techniciens and 40 engineers down at the assembly lines.
* on the assembly floor, 1 technicien + 1 electricien per module. They work by 4 (2+2) on a line/machine. (not sure about this one)

They have one local HR department in Beringen dealing with the 750 employees working on the premisses. All local HR departments are coordinated with the central HR at the headquarters.

Concerning the employee population, they have about 10% women, mostly in controling, marketing and HR. There are really few women in technical. The visit of the assembly department in Beringen was very representative of this phenomenon: about 140 employees, 3 of whom are women (less than 3%). 
To promote women in technical jobs, Bosch has special programs, called _diversity program_. They don't have a kindergarden on the premises, but it is sponsoring kindergarden fees for those who need it.

Bosch has about 10% apprentices, which is quite high. It is also working with other companies like +GF+ to offer apprentices programs (something called *LIBILIA*?????). They don't usually work with schools, except at the master's degree (master thesis in engineering might be done in collaboration with Bosch).

People at Bosch don't easily go up the ladder: vertical promotion is not common (people are mostly hired for what they are good at so there is no point to move up). 
For a leading position, one need at least three years of experience in three different continents. This means that the applicants should habe lived with his family in the US, Europe and Asia --> at least 9 years abroad. 

