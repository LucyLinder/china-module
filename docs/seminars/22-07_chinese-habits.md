
The PDF is available [here](../resources/pdfs/22-07-chinese-history-and-culture-stone.pdf){: target="_blank"}
{: .alert-info }

## Dragons

__origin__: ancient Chinese lived in a hard condition and had a limited knowledge about nature. Different tribes had different totems which were believed to be able to protect them and avert disasters.
One tribe in Central China, which had a snake as its totem, conquered other tribes, thus adding parts of other totems to its snake totem; At last, the snake totem became a mixture image which is the present dragon.

In the old days, the dragon could control wind and rain, so it was very powerful. I can change its form, but has a list of appearance attributes... Can be confusing.
It became the meaning of authorities in the old days.

Now, young people are using the western Zodiac signs and are slowly forgetting the chinese one.

Rich people put the nine sons of the dragon on their roof, while poorer ones can only afford one.

## Religions

Many different religions, even though most people have no religion. The most popular are taoism, buddhism, islam, catholicim and christianity. 

They are very open-minded. For example, some temples are a mixture of taoism, buddhism and confucianism.

They belief system is also very complicated and confusing. 

One native religion of China is taoism, which can be summed up with "_don't do anything_". One of the reasons of the way chinese people tend to ignore problems and walk around it.

> “wuwei” (non-action): means to let things develop in their own way and to avoid actions against the nature.

_Confucianism_: first the leaders, then the family. It is why in here, the boss asks you to work overtime, you do it. In Europe it is quite the opposite: the family is more important, so your family's obligation come first. You can refuse overtime in order to see your child play soccer, and it is perfectly fine.

## other

In banquets, there is always a target person that will be pushed to dring and finish completely drunk. Often, they are all around you and will make cheers one by one. After three rounds, it is more than 30 shots... !

_Jiu_ is the general term for alcohol.
To toast with a boss, you need to put your glass a little bit lower.

Different teas come with different pots.

The Mid-Autumn festival in October-November: some hotels already selling the kits for the Moon Cake, which is a big deal. The packaging is complicated.

Now, being kidnapped and sold to a vilager still happends. Blind Mountain, the movie about one of these stories is not allowed in China. But the remake were the abducted woman becomes a beautiful and kind teacher in the village is allowed :).

~30% of college students go to university.


6x3 meters is kind of a standard for rural houses. Dormitories are very bad in our standards, but if you come from the village, they are pretty good: toilets, water, ... better than what they have back home.


In the South, vegetable and rice. In the North, some eat only dumplings or only noodles at every meal (with nothing else).

More than thousands coal power plants in China ! Everywhere.

The vespa-like scooter we saw were actually fully electric bikes.

Since shared bikes are not their own, this is really a mess especially around metro stations.

Google play services are disabled in China. Instead, huawei store, etc. You can install Google Play from them, but it is not the same. So forget it !
Huawei is very hard to root.