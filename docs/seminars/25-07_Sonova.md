* now, 80% of sales is the RIC (Receiver In Canal), which has both the advantages of the BTE and ITE.
* now, platform 4.0
* SONOVA is a group, wich regroups Unitron and Phonak
* Suzhou is the biggest factory
* sales companies/distributors: company based mostly on manufacture, the distributors then take care of reaching the final clients
* #1 hearing aid manufacturer / hearing solutions supplier
* ISO9001:2003 & ISO13485:2003 (quality of medical devices). Plan to pass the 2016 version ? ISO:14001:2012
* Honesty Excellence Unit and High-Tech enterprise award from the FDA & government every year
* "success factor": low-cost good workds (quality is made by people, not machine)
* turnover rate very low, even lower than Switzerland. 
* > 98 deliveries on time
* They only do the assembly: the PCB and such are imported


_Quality-first_:

* ISO 13485, Japal, UL, CSA -> quality related certificates are all fully implemented
* monthly employee award
* CIT: Coninuous Improvement Team to redue the waste and much


## The visit

The salary for assembly line workers is about 3'000 CHF a year, which is less than at GSS (3'500 - 4'500). The fahrer from Shanghai, the cheapest !
Each worker has a name plate (called a _toblerone_) to give them individuality. Each has its own cushion or blanket to make the station more confortable. A huge place with about 200 workers, no automatisation. They solder, use microscopes, check, etc. 

low fluctuation rate: also comes from the good conditions. Cleanliness, air-conditionning (first for the material, but still), good lighting (not one lamp per person, but good light from the ceiling, which is more expensive but better), very low noise environment (something we often forget to check in CH) ... -> better conditions than home.

The production doubled every three years without hiring new people. How ? Continuous improvement, processes changes and also a steep learning curve (people become more efficient, good training). There is also individual or group bonuses for efficiency and quality.

This zone is a sort of duty-free: free of charges and taxes for all companies in the park, but only if the companies do ONLY import/export (i.e. import, asembly, shipping). If they want to sell to China as well, need to first export to Japan for example. The taxes are only on the margins the enterprise makes.

__100% of the production is made here__. This was a risky bet: unstable politics, etc. 

99% of the workers are women. They seem to better handle this kind of job, with precision and high concentration for a long time.

The last room is for repairs: here, only skilled workers knowing the full process. __70'000 pieces per week__ is roughly 12'000 piece a day, about 600 of which need repair/rework every day.

Their stock is huge (not lean at all), but they still manage __a lead time of 3 days__, not bad at all !

Since 2009, also opened a _repair service_: service exchange, not repair. 


### about quality

Quality of 98%, which is quite low ! But almost 0 (< 1%) reject rate.
Modules are tailored-made, so complicated. Also, there is a tradeoff between the product performance and the production cost. They want to keep the flexibility to improve and change the product without constraint, which makes an automatised assembly impossible. Result: hand work everywhere and rework at the end after functional tests for about 3% of the pieces. Big, but accepted and difficult to do better with this kind of assembly process (hand).

Variance, model change, very small peices, constant model switches -> the price of automation would be too high, need a lot of flexibility. Always some handwork, always some rework at the end.

Sub-assemblies are not checked, only final tests.
Quality and efficiency (group) bonuses.

The tests are done in the end. One machine for testing the efficiency of the device given different frequencies, but afterwards all functional tests are done by human/women. One women will do all the testing for a given device. 
If something wrong detected, the device is reopened and repaired. Usually, one repair is enough to correct the problem (small adjustments).
The testing includes metal noises, undesirable noises, scratches and visual aspect, etc. so it is difficult to replace the process by machines. Indeed, how to assess the quality of a sound with a machine ? Hard and expensive !
One interesting thing: the listening tests need well-trained people with a good hearing and able to follow strictly the protocols/respsect the quality goals. This was one of the main questions when moving to China: are chinese able to handle this kind of tasks ? They doubted it, but it seems they can perfectly do that (recall: < 1% return rate).

One of these apparal is about 12'000 CHF, a high-tech product.

---------------------

_Asian/Chinese and especially women better at this hand work. Why ?_

Comes down to the generations: old generation better and more flexible. Also, the chopsticks help (seriously). Younger ladies don't have this capabilities anymore. Also maybe the devotion, which helps concentration and haptic work. Smaller fingers also.

_Automatised test_

Quality of a sound is not easy to detect by a machine or if it were possible would be more expensive. People are not here only to test, but also to verify that all the functions are all available: remote controling, visual tests, etc.

_Increase performance_

Continuous improvement of the processes, workers are more performant, lean. Workers have a target a day and efficiency bonuses.