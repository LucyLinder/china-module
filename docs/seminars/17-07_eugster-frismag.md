# Eugster Frismag

Eugster Frismag is a family-owned company coffee machine maker. They have no brand, but are one of the five companies in the world doing coffee machines. They work for many known brands such as Jura, Nespresso or Starbucks. The invoice structure follows the following path: EF electrical Shenzhen -> ED electrical China -> Eugster Frismag CH. In Shenzhen, they have good deals with the government, so it is possible to reduce the costs at a minimum at the base of the pyramid.

* have 5 production plants in Switzerland, China and Portugal
* is one of the world's leader in their field
* have been in Shenzhen for 10 years; about 1'500 employees, only 7 expats. 
* Chinese women can be high up in the hierarchy (the head of HR and finances were chinese women)
* have a peak season in August-September to prepare for Christmas
* In the future, they want to build new buildings of their own. Right now, most of the buildings are rented. Hopefully, it will be fast, thanks to the "china-way" (building something is a matter of weeks, not years).
* one coffee machine is about _$30_ to produce. When adding margin and brand, it is sold at about _$120_ 

In general, they design the CM with the client, then launch the production in a _just in time_ manner. They try to automate most of the processes, for example the milling machines and the measuring robots. But as we saw during the tour, human interaction is everywhere: from the molding machines (people putting the heating unit into the mold by hand) to the screwing of many elements and wiring, etc. They are trying to change that and to automate more, since automation means reducing the production cost and thus increasing the margin. Indeed, _workforce in Shenzhen is more expensive than in East Europe_ for example. Anyhow, salaries are low in our standards for regular workers; for management salaries are aligning more and more. 

## About the plant in Shenzhen / China

In Shenzhen, they produce heating elements, pumping systems and many small part, as well as assembly whole coffee machines (we had a glimpse of _Nespresso citiz chrome_ ready to ship during the visit). They usually pass through Hong Kong for shipping because HK is more relax. In China, containers can be retained for more than two weeks if you forgot to declare something or just made a mistake (on purpose or not).

In China, they have an R&D department of 50 people (kind of the same as in CH), so they are able to design an entire new coffee machine in China if needed. This is pretty rare, since most of the time production chains are directly imported from Switzerland (see Geberit for example).

They have dormitories for their workers, since having the dorms inside the factory is not allowed. The problem is, only top 500 world companies can buy land in Shenzhen. They nevertheless managed to get some, thanks to _guanxi_: the manager offered dinners, cigars, etc. to the mayer and show him he was nice and serious. It worked. 

8 assembly lines -> able to produce 8 different models at a time. 

Some of the high end/hign tech products are still done (or at least assembled) in Switzerland. They really want to preserve the jobs in CH too. It also seems that delivery time might sometimes be a problem and that it is better to keep some product in CH to justify the prices. If everything was done in China, the prices would drop worldwide, meaning less money for everybody. 
Anyhow, the "swiss made" label does not enter into account. 


The plastic and raw materials are mainly from Europe. The reason: not everything can be found here, or at least not at the same quality.

They have 3 apprentices per year (3 years of training). They currently don't have any support from the government but will try to get some in the future. Maybe not money, but at least some benefits.

## About the China mentality

People in China think differently. We are very straight-forward: we see a problem, we find a solution, done. In China, people will try to find a way to avoid or get around the problem. They will also look at it from all the possible perspectives and way, making the thought-process very rich and creative. This also comes from their language: in Western countries, we are very logical: letters form words etc. In China, they deal with pictures: a world is an association of images that represent the new stuff. This is way more creative !
In short, we are straight but consider only a few factors. They are the opposite. So combining both can be a very good thing (but for that we need to form them to speak up).

Way of life: _"me first"_ versus _"us together"_. Everybody knows everybody, so it is difficult if not impossible to keep a secret in China. 

People are constantly on the move (marriage, children at home, etc). This is the social system: young people go to work and send all their money back, elders take care of the children back in the countryside. 
This is why people are working so hard: they have nothing here and no money to waste. The result: they want to work and overtime is a blessing -> __12 hours 6 days a week__ (they have nothing else to do, they just want to work/gain money as much as possible). They problem is, the China law _limits overtime to 36 hours per month_...
So, people need overtime hours but are not able to do so. The result: many companies just have two books: one for official hours, one for real hours... EF for example always fails the inspection and are currently considering faking it as well.

The chinese attitude: you ask for something, they do it. Immediately. No delay. No procrastination. How much you can do in a short amount of time is thus incredible !

* Strikes can happen
* they say they do strong security checks (IDs and age) to avoid trouble with the law
* the turnover is low, everything is even too stable (??)
* Chinese people smoke more than us. Actually, they stopped smoking in some places like hospitals and restaurant not so long ago...

## About the quality

They are certified ISO14001:2004 (environment) and ISO9001 (quality management system).

The software for measurment are shared accross all plants, meaning that they expect the same quality in China than in Switzerland, which is not a small task.

For example, they installed a water treatment station with the goal of zero-waste water. Funny thing is, the government couldn't believe it was a genuine goal and asked them continuously for month why, trying to find the trick. They couldn't believe a company would really want such a thing if there was no direct advantage whatsoever.

Relations in the government is the most important thing when opening a factory in China. This is called _"greasing the wheel"_ and it is present everywhere. But in China, _guanxi_ is at the center of many things.

> chinese are creative, but precision and thouroughness is lacking. We are here to ensure everything is the same, not similar. Perfection and the look for details is not in their culture [...So] quality is harder to achieve here.

Tests are done on each part at every step of the line. In the end, each machine gets a functional test. Finally, 3% of each machine in a palet is taken for a full test suite. 

## About the concurrency

As said before, there are about 5 companies doing CM worldwide. 

A coffee machine is very complex. Components are exposed to 40 bars of pressure; a CM is usually constantly on, so a bad contact or a small detail can make it get on fire after a while. Mold-making is also science. In short, making a sustainable coffee machine is tricky and China could be able to copy the parts but would not easily make the whole lot work.

China is heavily copying. Copies flood the inner market, but they have difficulties reaching the outside market: quality is a problem, as well as copyright and such.

From the perspective of the speaker, the real thread is 3D printing. 

## The visit

Management techniques and such: 

* hourly production reports on the walls, but not really up-to-date
* 5S (pictures of the tool shed on the walls)
* explanations on the wall with what is a good and a defect/bad piece and what to watch out for
* line managers have their pictures on the machines/areas
* the "_6 golden rules_" are on the walls and the "_vision 2020_" diaporama is on the many lockscreen throughout the factory, but mostly in english... And not as pregnant as in Geberit

The noise is deafening. Air conditioning is not in every room. If not, there is one fan for each workstation.

On the assembly line, each woman (I say woman because there are like 90% of women in blue) has a little pedal to make the parts move further when finished, so the rhythm is not imposed to them. They can stand up or sit as they whish.

There are about 0,015% of defectivee machines in the end, which is a very good percentage ! But to achieve this, many checks and tests all along the lines, with many discarded parts.. All in all, it requires more tests and more thorough tests to achieve the same level of quality in China than in Switzerland. It has also to do with the mentality: 

One floor was full of unused machines. These are only for the peaks --> waste of money ?

Each worker has a t-shirt with one of the following colors:
* _blue_: production workers (the one in the dorm, mainly migrants)
* _blue with white stripes_: new workers (less than one week)
* _orange_: warehouse
* _green_: test
* _red_: line managers

All in all, workstations and work conditions are good. The production lines are quite the same as in CH, maybe a bit less modern. Some things though:
* fans instead of air conditioning everywhere
* no bad positions (the back will be okay :) )
* women working with electronics have a wire for the ground, but some of them are not even wearing gloves 
* we saw a worker with tongs ! (it was supposedly not from the company but an external worker though...)
* the protections are way less thorough than in CH: no real shields for many machines, gloves/glasses, etc. are not always present, women put their hands very near cutting machines, etc.

They showed us a pumping unit (max. 19 bars, with electronic to control the pressure depending on the coffee) that was done here. The strange thing: there was a label with "_SYSKO_" written on it. When a guy from the group asked about it, the answer was really not clear. Did they produce it but changed the name ? Did they import just the part with the label ? Did they lie and don't actually produce the pumping unit ?? This was answering in the chinese way :). But is seems that SYSKO is actually producing stainless steel, so I might be the second alternative: just the metal part was from the outside.


The molds are produced by another company, but they take care of the repairs by themselves.



__about the language__: I noticed that many written stuffs were just in english. Most importantly, the interface for the softwares (for example the screen for testing) is only in english --> the use the color of the buttons to know were to press, but don't understand a thing. As the guide explained, this is a common problem: the company makes a software in the West and then ship it with the rest of the production processes to China. They never think of the language ahead. The keyboards are QWERTY.

Anyhow, the company policy is to have everything both in english and in chinese (didn't see it everywhere though). And for important informations, they use _WeChat_: everybody has wechat in the enterprise, and low workers are informed on important news by this channel.

## Miscellaneous

* Nespresso is about 7'000'000 machines a year from 3 different producers
* UG software and AM3 instead of SAP 


## Sustainability
- vision à long terme, 
- dépendence à la suisse - sont-ils able to produce by themselves,
- environment
-  people like to move (from and to the countryside), there is a lot of turnover (see how many new employee with white lines there were) --> loss of time
-  postes sur les assembly sont faciles à assembler (alu, etc) et à modifier donc facile à modifier et flexible --> adaptations very fast
-  sécurité: un problème pour la durabilité ?
-  et surtout: beaucoup de pièces defectueuses, besoin de multiplier les controles tout au long de la ligne pour avoir lâ meme qualité qu'en suisse
-  