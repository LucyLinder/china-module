The PDF is available [here](../resources/pdfs/10-07_02-container-logistics.pdf){: target="_blank"}.

Growth in container volumes has exceeded 10% annually within the last 15 years and will continue to grow.

The biggest ships today are oil tanker ships. The container ships began in the 50s-60s, they were very small. Now, it's more than 100 meters long and about 30 meters high (80 meters when loaded).

see [the world biggest container ship: the majestic Maersk](https://www.youtube.com/watch?v=-MspFZpwMkk){: target="_blank"} video.

The autonomous zero emission feeder: a project that maybe gives us a glimpse of the futur. It is completely autonomous, with no driver.

## Container terminals

They allow the transfert of intermodal cargo containers between ships, trucks and rail. The difficulty is to optimize everything with good logistics. It must be efficient, fast and clever ! 

Needs for container terminals:

 - a lot of land
 - a lot of technology to move containers
 - a lot of logistics

To have a glimps at the difficulty, see the video [Singapore container terminal](https://www.youtube.com/watch?v=HrZg96L8yaY){: target="_blank"}:

<iframe width="640" height="360" src="https://www.youtube.com/embed/HrZg96L8yaY" frameborder="0" allowfullscreen></iframe>

Even the charging of the same ship is difficult. For example, if the ship goes to 3 locations, it is important not to put the containers for the first stop at the bottom, with containers for other locations at the top. To do this, you also need to plan which truck comes first, etc. The optimization goes way before the charging of the ship! Optimisation is also in the number of moves of the machines and the trucks, the location of each container on the dock, etc.

__Teu__:  stands for _Twenty-Foot Equivalent Unit_ which can be used to measure a ship's cargo carrying capacity. 

## Container logistics

container-transport chain: 

![container transport chain, wikipedia](https://upload.wikimedia.org/wikipedia/commons/5/55/Schematic_container-transport_chain.png){: style="max-width: 600px;"}

The [paper nightmare](https://www.youtube.com/watch?v=p8yH4e-Aafk){: target="_blank"} video:

<iframe width="640" height="360" src="https://www.youtube.com/embed/p8yH4e-Aafk" frameborder="0" allowfullscreen></iframe>

Transport modes: 

* _deep-sea vessels_: intercontinental, long distance
* _short-sea vessels_: move inside the regional area, for example between countries of the same continent
* _feeder vessels_
* _barges_: small vessels that serve the hinterland of a seaport via rivers and channels
* _XTs_
* _Trains_


__barge vs vessel__: The key difference between barge and vessel is their route: vessels can be seen in both inland waterways and international waters whereas barges are only seen in inland waterways.

So, a _barge_ is a long, spacious, flat-bottomed boat that is used to transport goods on inland waterways such as rivers and canals. Some cannot propel on their own; they are towed or pushed by tow boards.

## In China

__HK__: distribution to USA, Asia and Europe, but also to other parts of inner China.

__Shanghai__: the cranes are more than 50 meters high. See the [East Container Terminal](http://www.apmterminals.com/operations/asia-pacific/shanghai){: target="_blank"} website. The infrastructure is split between companies or countries, each one managing its little part of the dock. The docks are leased to companies, but the land still belongs to the government.


## Container Terminal & Operations

> A container terminal is a complex system that functions only efficiently when its layout is designed in such a way that the loading and discharging process of vessels runs smoothly.

Problem split into sub-problems: 
1. loading/unloading (vessels, barges)
2. storing
3. receiving/delivering (trucks, trains, etc.)

![3 areas of terminal operations](http://ars.els-cdn.com/content/image/1-s2.0-S1366554515000721-gr1.jpg){: style="max-width: 400px"}

Example of terminal simulation ([video](https://www.youtube.com/watch?v=LfzKTR1gTvg){: target="_blank"}). See the program FlexSim for more simple simulation as well. Simulation can be very useful for optimization problems.

Vessels should stay at quai as less as possible because one day is about 50'000 $.


## The future ?

The keywords: more automation and better technology. 

Possibilities:

*  OR and VR + touch screens to better manage the dock
*  remote driver cranes
*  Drones monitoring 
*  RFIDs to track containers and monitor the dock
*  artificial intelligence, deep learning for optimisation
*  supply chain digitalization with IoT
*  blockchains for the security of information

The mass of information for a terminal is so huge that IoT and smart technologies would be of great use to simplify the whole workflow. BlockChains could also be used to secure the whole infrastructure of information.

## Miscenalleous

What we see now is a saturation: the building of new ships is stagning. 

BNCT, the port of Busan (Korea) is known as the first vertical-automated container terminal. Only the part in the middle -- the stack area -- is automated (were containers are stored before loading).

<iframe width="640" height="360" src="https://www.youtube.com/embed/2t2cqiQl0BE" frameborder="0" allowfullscreen></iframe>

From Asia to America: x2 between 2000 and 2010. x2.5 from Asia to Europe.

For the mediteranea to keep being attractive, it need more efficient infrastructures, mostly inside the land: trains are expensive and the transports from south to north is not very good. Consequence: sometimes it is better to use the sea to go from Italy to Belgium (west of Portugal, the Amsterdam) instead of going straight through the roads and trains.

Lately a Greek port has been acquired by a Chinese company. It is strategic: the terminal operators decides when which ship is taken care of. By owning the port, you get all the control. From Greece, they can control the mediteranean area (Greece is one of the entrypoint). So it is to gain the commercial control of Mediteranea.

China opened its door to European and American shipping lines and let them own part of their dock. This is very interesting: no one has full control, every company can do what it wants in its area. But China stay the sole owner of the land.
