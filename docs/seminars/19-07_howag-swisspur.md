
__Howag-Swisspur ltd.__

## Introduction 

* 1/3 to howag, 2/3 for Forster.
* 2/3 of the sells in made in China. Most of the clients are companies in the near area (they know there is a smaller defect-rate with Swiss know-how). 
* started in 2005, now 2'400 m2 and 85 employees
* manufacturing cable assembly
* low pressure for PCB and eletronics
* growing control panel business: they actually buy parts from China (except important parts that are imported from ABB) and sell them to Iran. Each panel is unique. Here, the real know-how is the buying: finding the right suppliers to get a good product at smaller costs -> the real know-how
* do the final product here
* ISO 9001/2015: prove to client that we have quality standards, since there are services PIC-A-620B (cable assembly cabe), etc.
* PIC-A-620B is a sort of picture book, which is ideal for China; it shows examples of what is right or wrong.
* direct (Bystronic, Robatech) and indirect clients (CCS, Kärcher)
* they won't go fully-automated. Since they produce between 10 to 50'000 pieces (but most of the time small quantities), the setup costs don't make any sense

This company is really good at Quality-planning and finding suppliers. They invest a lot of work in it, as well as in the testing of the components. 

They will focus on efficiency in the future, to compensate the rise of the labor cost. Also, since many clients are enterprises here in China, there is a risk that the latter will move to another country with smaller costs. They hope not.

> for foreigner companies, labor laws are even more strict. There are also controls from the government every once in a while.

## Company visit

* open concept, because they don't like walls :)
* one order is from 10 to 50'000 pieces (big orders are more rare)
* everybody wears masks for soldering. They are also really careful about security, because people could try to trisk them and sue them afterwards (for example, not declaring that they are pregnant or not signing the contract and then tell they weren't offered one)
* use PV cable instead of PCB, which is of lesser quality
* pretty much everything comes from China; but in the end, it really depends on the end product. If a cable will be plugged once or twice in its lifetime, no reason to produce a rolls-royce; cheaper materials will do the job
* tests are more complex because of the humidity: they need a special room (currently at about 38% humidity) and leave the cables about 24 hours before testing, to be sure the tests are correct for high voltage (300 V)
* They produced all the cables for the trottinettes _micro_

> We prefer ladies, they are more loyal.
