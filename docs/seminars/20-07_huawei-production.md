The production site is located at Songshan Lake, Shenzhen, where 10,000 people produce electronic equipment.

The Songshan Lake Park is the _Silicon Valley_ of China. The park is located at the geometric center of Dongguan with a planned area of 72 square kilometers including a freshwater lake area of 8 square kilometers. 

## The visit

First, we needed to be well dressed and wear big blouses, caps and shoes with a thread connecting us to the ground. The whole visit was standardized: talks, tour, what we see, etc. So not very revealing.

The warehouse is fully automated, with a logistics center for raw materials robotised. The technology is not their own, but some of their production means are.

AGVs (automated guided vehicles) everywhere, lights above each machine to show problems, Windows operating system on the test machines. The tests are fully automated as well, human presence is sparse. 

The whole functional testing system is home-made (hardware and software).

Of course, all this automation is mostly for showing: in the smartphone area, most of the work is done manually (also because it is constantly updated: it is impossible to be that flexible with full automation).

* security gates: mostly to avoid employees stealing the product (so when you go in, no problem)

The product we saw on production:

![product we saw](http://www.telecommunicationbts.com/photo/pc5679412-telecom_base_station_huawei_bts_3900_grfu_1800a_21023185466tba001025.jpg)

PCB -> putting components -> checks -> soldering components -> checks -> casing -> functional tests.

What we see is what they want you to see. The number of people was very low in this part of the production, but if we look at the number of people and the surface of the plant, it is clear than most of the other processes are not that automated. 

We are not worth anything for them, so they give us nothing. If we were real potential investors, then we could have had the possiblity to see more. 
Also, the woman responsible for the tour seemed very angry and bored. We had a strange  conversation: 

* "I don't understand why you are there"
* "we are learning about China and their production process"
* "Ok. So how could be improve ? What can you say about Huawei ?"
* silence. We were here for about 30 minutes and saw pretty much nothing. 
* "I still don't understand what you are doing here. Let's go !"
