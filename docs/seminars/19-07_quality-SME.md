# Is Quality in Asia a question of culture ?
Presented by Rene Forster.

Mr Foster spent many years in Asia, in Indonesia, Sri Lanka and Guangzhou. Electrical background, with an MBA in management. Opened his own company in China in 2005.

## Barriers

* language: the biggest problem !
* how we talk and react: we are loud, upset, angry, which can be misunderstood
* decision: it is important for other to understand your decision and to be part of the process
* acceptance: creating links of trust with suppliers for example

Simple process: 1. understand culture, 2. precautions. The culture is similar to an iceberg. Behind what we see are beliefs, traditions, values, etc.

Swiss people are very safe in their problem solving process: you understand, decomponse, talk, etc. but it also makes the process very slow
{: .alert-info }

## Hofstede culture indexes

_Geert Hofstede (89)_ wrote "cultures and organizations" which describe how cultures are different or similar (from an enterprise point of view) all over the world. The study was made in IBM. For him, six different dimensions of culture:

* power distance
* individualism
* feminine/masculine
* uncertainty avoidance
* long-term orientation
* restraint/indulgence

The first two are the most interesting for us.

### low vs high power distance index (PDI)

CH or Australia have a low PDI: we consider ourselves as pretty much equal to our bosses. In Asia, this is the exact opposite: it is more autocratic, the boss is sacred. 

* higher PDI means more inequality
* people in high PDI cultures are more likely to accept their inequality

Examples of PDI index values (out of 100):

* Saoudi Arabia 80, 
* Indonesia 78,
* CH 34,
* Austria 11

China was not on the map in 89, but it should be about the same as Indonesia.

### Individualism Index Value (IDV)

Individualist cultures, people are expected to develop and display their individual personalitites.

Collectivitst cultures, people are defined and act mostly through the group, the community and the family.

Examples of IDV index values:

* Italiy 76
* CH 68
* Portugal 27
* Indonesia 14

In Indonesia, friends touch you and it is not a gay thing, that is normal. Anectdote: the guy goes to the bathroom and gets a massage while doing his business. It would be a violation of privacy in CH !

## China vs CH

Power distance: 80 vs 34. Individualism: 20 vs 68 --> this is a gap. For uncertainty avoidance, China = 30 and CH = 58. 

Putting PDI and DVI into a graph, we get nice clusters (TODO find it on the internet).

The _CultureCompass_ App let you compare two countries in terms of enterprise hierarchy. You should try.
{ .alert-info }

## Inglehart-Welzel cultural map

There are two dimensions of cross-cultural variation in the world:

* x-axis: survival values versus self-expression values
* y-axis: traditional values versus secular-rational values

Used a similar approach as Hofsteded with questionaires. 

Once again, they made a photo once, but this map will change every 5 or 10 years with modernisation and permeability between cultures.

## Culture comparison

| West          | East          |
|---------------|---------------|
| grow up alone | grow up in a group |
| "I", "me"| "we" ** |
| school, problem solving| school, memorizing |
| diploma, financial | diploma, status |
| management, individualist | management, group |
| task before relation | relation before task |

** !! in China, the "we" is only in family. Once outside, the I is even stronger than in CH

## Common sense

* judgment not based on specialized knowledge, but a native good judgment depending on social and cultural environment, how we have been educated. Common sense also shapes the conventions and non-spoken rules we obey.

Basically, we have the _human nature_ , which we inherit, then the _culture_ (learned) and the _personality_ (inherited and learned). Common sense is both culture and personality. 

We often hear "_these chinese have no common sense_". That is not true; they just have a different one. 

Chinese spit, talk loud (even scream on the phone, but they really think that the other will better hear that way).

## preventive measures

Read, inform yourself on the other culture, try to understand it. Learn the language (the guy is here for 15 years and can't read, only have small talk and order in restaurant).

To learn Chinese, you really need to be young and to practice/use it everyday, hopefully with a boyfriend who doesn't speak english. It is very, very hard.

> Culture is an onion and the outer layer is the language. 

In China, you must avoid losing or let another one lose face. Someone angry will give you a resignation, but won't come talk with you. Everything is kept inside.

Decisions must be understandable for employees. You have to tell them why, almost to hold the hand (explain why it is like that).

As the boss, you are very much included in their lives and important.

## Yiang Liu

see [her website](www.yangliudesign.com). This is where the nice graphics about our and asiatic culture is different.

asia: the most business card, the better. Doing the queue is changing, they don't tolerate people cutting the line anymore.

The way of walking around the problem is also changing a bit now.

Chinese love to do sport and to keep in shape.

You can now have two children.

## Q&A

_is it difficult to find people speaking english ?_ It was, but it isn't anymore. More and more young people learn english.

_westerner try more to understand chinese or the other way around?_ Chinese will try to accomodate only if they see that they can make money. At the end of the day, they have their culture.

_Regional differences in China?_ Many stereotypes about people for this or that region. The way they think here and in Shanghai is also quite different. More formals here. 

The guy explained to his employees and formed them to accept and recognize a problem. "I won't be angry, but to solve a problem we need to see it so tell me !"