## Seminar 1: Engineering practice within chinese and swiss culture

The PDFs are available [here](../resources/pdfs/10-07_01-intro-economy.pdf){: target="_blank"}

In year 1, China was the second biggest economic power, right after India. We were third. China has been at the first place many times mostly in the 1600s and 1800s. At the beginning of the 20th century, USA became the center of the world, and China dropped down the chart. It is mostly related to the communism and World War II. It is coming back in the 21th century and many scholars predict China to become the first economy again by the year 2030.

In Shanghai, the density is 3'600 people by square kilometer ! In Switzerland, it's about 188/km2.

In 1970s, famines were common and people starved by lack of food. Now, the middle and upper class is much bigger and the consumer power has risen amazingly. The poor population was about 36% in 2000 and dropped to 10% by 2010.

![number of urgan housholds by annual household income](resources/graph-household-income.png)

Mercedes: one out of three cars is sold in China. It's a huge market ! 

In China, export is pretty much the same is import, which is really different from say Germany (export is much higher). China is not the bad guy spoiling the market: if the Americans are importing a lot, it is not just because of the Chinese.

The growth rate in Switzerland is roughly 1%. In China, they are at about 5-6% ! This is really an economy to deal with in this generation. As a supplier, consumer, competitor, etc. The center of power is shifting towards India, Brezil and China.

> The music is playing in China. You should listen to this music, because it will influence your life whether you like it or not. We now look East instead of West; it is both your opportunity and your threat.

In 2014, China was second in term of gross domestic product: $10 trillions against $17 trillions for the US. The same goes for the number of billionaires.

![graph-efficiency-innovation-economy.png](resources/graph-efficiency-innovation-economy.png){: title="From efficiency to innovation-driven economy" }

Astonishingly, Switzerland has the same grade for health and education... But we are far more advanced in infrastructure.

China is investing huge amounts of money into R&D and scientific researches. In innovation, Switzerland is quite good, but we need to watch our back.

> If you are on a Safari, you shouldn't be faster than the Lion, you should just not be the slower in your group.
