* The Chinese ambassy is the most important of the Swiss ambassies around the world.
* many visits from officials between the two countries
* in charge of papers for expats, promoting exchanges between countries, helping Swiss companies to develop in China
* Hong Kong still one of the anchors to the Chinese world; 57% of Swiss Citizens live in HK in 2016; some families have been there for 30 years, so not just a few years and back
* FTA: Foreign Trade Agreement is a current project; regular meetings to rebiew and negotiate the details
* you need to be ready and flexible in here, because anything can change overnight

_The Yangtze Delta Region_: 23% of GDP, 10% of the population, 2% of the total land area.

_Shanghai_ 

* 24.3 million people (officially) and about 6'340 square kilometers -> the same surface than the canton of Bern
* \#1 in import/export in mainland China (huge port)
* \#4 of cruise ships in the world.
* well-ranked universitites, many global companies (VW, etc.), ... Have the potential to really do innovation (which is a keyword for the future of Shanghai); a direct competitor with HK for the financial center; another plan for the future is to go abroad, i.e. to buy foreign companies (e.g. cingenta)

Why going in Shanghai vs Shenzhen ? Shanghai is more for the third tier, for services. Shenzhen more for second tier. But in this end, it always comes down to what is your product an your target. Shanghai has 24M people, Shenzhen has 11M. Shenzhen is a huge industrial area with many suppliers, etc.

The legal system here is not codified like in other countries. Regulations and laws here, but not all are very transparent. 

> "_the rule of law_" (the laws govern the nation) vs "_the rule by law_" (the ind)
>
>“’Rule of law’ implies fairness and predictable application,” he says. “’Rule by law’ would include, for example, rule under Hitler’s Nuremberg Laws (Nürnberger Gesetze), which were neither fair nor predictably applied.”

> It’s an important distinction in China, where courts, police and prosecutors are controlled by the Communist Party and where the constitution — which guarantees freedom of speech and religion, among other liberties – has been shunted aside when it conflicts with party interests.
> ([source](https://blogs.wsj.com/chinarealtime/2014/10/20/rule-of-law-or-rule-by-law-in-china-a-preposition-makes-all-the-difference/){: target="_blank"} )


The lawyers and judges use the low serve their interests, so you cannot always predict what will happen. This is a risk that some companies do not want to take.

## Swissnex China at a glance

(Isable Götz)

Opened here in 2008, so not really known yet.

Bring innovation, the "beyond chocolate" part. We are more of Tennis and coffee -> going to universities, companies, fairs, to promote Switzerland. 
For example, booth in universities to promote exchanges with Swiss universities. 

Organise Swiss universities ALUMNI meetings -> networking. 35-40 groups that come visit them per year. 

_Events_: 

* Joint Master program this summer, 
* China Hardware Innovation Camp, 
* venture leaders (10 selected startup from CH drown to China to find parters, investors, counseling, etc.).
* ...

_Global services_: help big companies for example. La Poste is on its tour right now to discover Asia and Asian opportunities.

> In China, people are always glued to their phones. In villages, you also have a sort of mobile-phone hardcore use lane !

So promoting online is a very big deal (and swissnex can have mandates to take care of this).


Not only promoting Shanghai and China, but connecting the dots in Asia and promoting it in a bigger picture.

If you are labeled CTI, you can benefit from a Startup bootcamp from Swissnex for example. Finding office space or legal advice is not their job, they are more here for network.

One big problem: Swiss want to plan three years ahead, Chinese don't even know what they want for breakfast tomorrow.


## Chinese higher education landscape

(Shi Bodong, "Stone")

* 2442 universitites and college
* times ago, about one thousand of university with 3 years college moved to 4 year Bachelor. Then, they found it was not a good idea and they reverse it/named it "univesity of applied sciences"... 
* C60 are college with a strong bond to the German college.

Too much theoretical. Now, they try to make it more about practice in the "senior/vocational secondary". If you come from the countryside, you will receive some money and are encouraged to do it.

primary education: not before 6.


__211 project__ and __985 project__ : a network of "best" universities, with help from the government. Before, in job hunting everybody stated clearly I want someone from that kind of school. But now, not allowed anymore.

Most Chinese going to Switzerland go for Phd or post-doc (which don't always register to the ambassy, so the numbers might be wrong). Many do prefer going to an English speaking country. 

Chinese people can easily get a double degree (2 years Cambridge for example), but not possible the other way around.

In China, to have the degree, you HAVE TO do your 3 or 4 years. To do a joint degree, you need to register to the chinese university one year before and being accounted to doing the courses here, even though you are still in CH so that in the end you have administratively done your 3/4 years.

## About the quality presentation of Forster

The "I" vs the "WE": yesterday, we asked for two keys for the hotel rooms. In there, not normal -- you just go up and down together.

Finding reliable suppliers: Stone didn't check the hotel before... Also, about "difficulty in measuring": we think it stinks, but when they arrive they say it's ok. The toilets --> "maintenance" ! Don't trust pictures...
If you are not satisfied, find a big group, yell and it will make things moving --> make them loose face.
"expect the worst"