Swiss Centers China - Biggest cluster of Swiss companies in Asia.

## Swiss Center China - Presentation

* 2 sites in Shanghai, here is were they started their activity 5 years ago
* provide companies practical support (hardware, offices, warehouses, showrooms) for their start in China. After the smooth start, will move away by themselves;
* 7 Swiss centers, more a trading platform
* a private foundation based in Neuchatel (vs swissnex), but get also some financial help from the government and sponsored by Cantons
* most of their team in China
* more into the machinary equipment and tools at first, now a shift to other industries such as retail (Migros - brands sold in China), automotive, ...
* employee hosting service: business come here, rent an office and can hire an employee under the name of the Swiss center -> no need to register a company to work
* trading experts able to give advices about setting a company, import/export and such
* took care of solar impulse in social medias (mostly WeChat)

_Main issues for companies in China_: HR - finding the talent at a proper cost (increasing in Shanghai). Many CH companies are in a niche, so difficult and there is a lot of investment first to find the talent, then to keep it (when switching, they can get a 10-20% increase).
Partners to find employee, Swiss Centers are not doing the search by themselves. The partners depends on the industry.

In Shanghai for an engineer, the salary can be about 3'000 CHF/month. If they have experience abroad and speak multiple languages, the salary can be even higher than in Switzerland (because it is rare!) -> expats are cheaper.

SwissNex is here to connect people, so very complementary.

## Chinese economy

One of the reasons they are #2 is thanks to the infrastructures. For example, connecting cities together (roads, high speed trains, ...). This is way better than in America where you have roads, that's it.

Now, service very low. But they are learning very quickly and it is getting better especially in 4-5 starts hotels.

In the 18 century, China was #1. Leading the world ! UK envoy to begin trading. The emperor invited him to a nice dinner and in the end, the emperor said "thanks, but we need nothing from you; we already have everything". UK thus found another way to force itself in China through the opium war.

The forecast is China #1 by 2030.

1.4 billion people -> many brands that we never heard of. Many big players in China don't need to learn english, they have everything at home.

Challenges: pollution, companies shift to produce in East Asia, corruptions.

Every 5 years the central government releases the master plan and objectives for the next 5 years, the __5 years plan__. Development plans, but also legitimacy plans. Now, beside the environment and innovation, corruption is also a target. 
Anti-corruption measures have been applied; the central government needed to ensure every little local government was effectively applying the measures. For exemple, change of offices: big beautiful buros -> open space offices.
The watch industry and some other luxury goods dropped because officials couldn't buy expensive watches and stuff anymore. A proof that something is really made.

Now, your chinese commpany need to prove that you are green. When you talk about people, they don't usually complain about the government, but the complain about the environment. For example, 1-child policy, child often sent abroad for the air quality, the food safety and the fear of the bad environment. 
_Stone_: a real change in the communication about the environment, but actual changes not seen yet.  

In China, when you buy a car, you need the plates. Shanghai, you begin with a lotery and if you have the number, you will also need to pay. If you go for EV, you don't need to pay the plate anymore. 

Uber: leader of sharing car system, very strong in Europe. When they entered in China, found out a chinese app was already copying with a better system: give smartphones to drivers and also a discount to the customer and the driver if they use their app. Now, they have bought Uber.

WeChat copied a model, but integrated a lot of services in it: facebook, twitter, whatsapp, invoice payment, etc. In one blink, whatsapp was dead. In China, everything goes fast and companies don't last 100 years.

Swatch group developped a smart watch only for China (no touch payment). So a good strategy is local innovation, then incremental publication.

There is a large room for high tech equipment in China -> an opportunity for us.

Switzerland exports: Germany > US > China/Hong Kong. China is extremely important for us.

--> __sino-swiss trade agreement__ <--

The fear of Swiss companies in 2016: first the economy slowdown, then the fierce competition ! After that, mostly the raise of the labor cost. The internal difficulties are mainly finding talent. 
Before, it was the lack of support from the head office. Because the market here is so different and the HQ sometimes has difficulties to understand that a business in China works differently.

## Pixi presentation

* "PIXI Mobile visualization"
* Pixi HQ in Switzerland
* HMI - Human Machine Interface in the railway business. Control panels for the drivers
* very small company but products in 5 continents
* no expat working here. Sales people come from CH 3/4 time per year, directors do the trip once a year
* the speaker himself was recruited through one of the partners of Swiss Center. For other employees, use another agency.

Only two production sites: CH and Shanghai. Why Shanghai ? The railway industry in China was starting to grow, so the sales people went 2/3 times a year to learn about the market, etc. They registered in 2007 and started with 2 people. Waited until 2009 to have really big orders and to make money. 

The main reason is that they are targeting the local market -> faster delivery and response time.

basically have the same products in both countries. But China want faster deliveries/response time and more special requirements.

Worked with suppliers step by step. There was some quality issues at first, but now they were able to get a good relation with trusted suppliers.